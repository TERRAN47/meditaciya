import React, {PureComponent} from "react";
import {
  Text,
  TextStyle,
  View,
  TouchableOpacity,
  ViewStyle,
} from "react-native";
import {Colors, CommonStyles, windowWidth, Fonts} from "../../core/theme";
import {styleSheetCreate} from "../utils";

interface IProps {
  genderSound: string
}

export class Arrators extends PureComponent<IProps> {
  render(): JSX.Element {
    const {genderSound} = this.props

    return (
      <View style={styles.view}>
        <Text style={CommonStyles.textBold}>Выбери проводника:</Text>
        <View style={styles.arratorsBlock}>
          <TouchableOpacity>
            <Text style={genderSound== "Men" ? [styles.arrator, styles.arratorActive] : styles.arrator}>МУЖСКОЙ ГОЛОС</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={genderSound == "Wom" ? [styles.arrator, styles.arratorActive] : styles.arrator}>ЖЕНСКИЙ ГОЛОС</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  view: {
    marginTop:15,
    width:"100%",
  } as ViewStyle,
  arratorsBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
  } as ViewStyle,
  arratorActive: {
    color:Colors.white,
    overflow:"hidden",
    borderColor: Colors.greenish,
    backgroundColor: Colors.greenish
  } as TextStyle,
  arrator: {
    paddingVertical: 10,
    paddingHorizontal: windowWidth * .06,
    borderRadius:20,
    borderWidth:2,
    borderColor: Colors.black,
    fontFamily:Fonts.regular,
    fontSize: windowWidth * .03
  } as TextStyle,
});