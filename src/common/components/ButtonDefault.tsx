import React, { PureComponent } from "react"
import {
  Text,
  View,
  TextStyle,
  ViewStyle
} from "react-native"
import { Colors, Fonts, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'

interface IProps {
  titleButton: string
  width?: number | string
  backgroundColor?: string
  color?: string
  isError?: boolean
}

interface IState {

}

export class ButtonDefault extends PureComponent<IProps, IState> {

  render(): JSX.Element {
    const { titleButton, color = Colors.black,  backgroundColor = Colors.white, width = "100%"  } = this.props

    return (
      <View style={[styles.buttonBackground, {width, backgroundColor} ]}>
        <Text style={[styles.buttonText, {color}]}>
          {titleButton}
        </Text>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  buttonText: {
    fontSize: windowWidth * .046,
    fontFamily: Fonts.regular
  } as TextStyle,
  buttonBackground: {
    fontSize: windowWidth * .046,
    borderRadius: windowWidth * .08,
    alignItems: 'center',
    justifyContent: 'center',
    height: windowWidth * .147,
  } as ViewStyle,

})