import React, { PureComponent } from "react"
import {
  Text,
  View,
  TextStyle,
  ImageStyle,
  Image,
  ViewStyle
} from "react-native"
import {Fonts, windowWidth, windowHeight, Colors } from '../../core/theme'
import { styleSheetCreate } from '../utils'
import {ImageResources} from "../ImageResources.g";
interface IProps {
  title?: string;
  color?: string;
  logo?: string;
  bg?: string;
  titleColor?: string;
}

interface IState {

}

export class WaveHeaders extends PureComponent<IProps, IState> {

  render(): JSX.Element {
    const { title, logo, titleColor, bg, color } = this.props

    return (
      <View style={styles.view}>
        {
          bg ?
            <View></View> 
          :
            <Image 
              source={color ? ImageResources.topLineBlue : ImageResources.topLineGray}  
              resizeMode="cover"
              style={styles.topLine}
            />
        }

        {
          title ?
            <Text style={[styles.textHeader, {color:titleColor ? titleColor : Colors.black}]}>{title}</Text>
          :
            <View style={styles.headerContainer}>
              <Image
                source={logo ? ImageResources.logoOrange : ImageResources.logoWhrite}
                resizeMode="contain"
                style={styles.logo}
              />
            </View>
        }
        
      </View>
    )
  }
}

const styles = styleSheetCreate({
  view:{
    zIndex:500
  } as ViewStyle,
  topLine:  {
    width: windowWidth,
    position:'absolute',
    height: windowHeight * .2,
    top:-10,
    left:0,
  } as ImageStyle,
  textHeader: {
    zIndex:100, 
    marginTop:30,
    fontFamily: Fonts.bold, 
    textAlign:"center",
    fontSize: 20,
  } as TextStyle,
  headerContainer: {
    marginTop: 5,
    zIndex:100,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  } as ViewStyle,
  logo: {
    width: windowHeight / 8,
    height: windowHeight / 8,
  } as ImageStyle,
})