import {NavigationStackScreenOptions} from "react-navigation";
import React from "react";

export function NoHeader(): NavigationStackScreenOptions | null {
  return ({
    header: <React.Fragment/>
  });
}
