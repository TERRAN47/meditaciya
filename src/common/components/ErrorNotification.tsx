import React, {PureComponent} from "react";
import {Text, TextStyle, View, ViewStyle} from "react-native";
import {Colors, Fonts} from "../../core/theme";
import {styleSheetCreate, styleSheetFlatten} from "../utils";

interface IProps {
    errorText: string;
    onOkPress: () => void;
}

export class ErrorNotification extends PureComponent<IProps> {
  private readonly newProperty = null;

  render(): JSX.Element | null {
    const errorText = this.props.errorText;

    if (errorText !== this.newProperty) {
      return (
        <View style={styles.container}>
          <Text style={styles.errorText} numberOfLines={2}>{errorText}</Text>
        </View>
      );
    } else {
      return null;
    }
  }
}

const commonText = {
    fontSize: 16,
    fontFamily: Fonts.regular,
    color: Colors.white,
    textAlign: "center",
    padding: 16,
} as TextStyle;

const styles = styleSheetCreate({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  } as ViewStyle,
  errorText: styleSheetFlatten([commonText, {flex: 1, color: Colors.paleRed}]) as TextStyle,
  okText: styleSheetFlatten([commonText]) as TextStyle,
});