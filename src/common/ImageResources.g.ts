/* tslint:disable */
import {ImageURISource} from "react-native";

export class ImageResources {
    static readonly image_back: ImageURISource = require("../../../resources/images/image_back.png");
    static readonly image_eye: ImageURISource = require("../../../resources/images/image_eye.png");
    static readonly image_eye_non: ImageURISource = require("../../../resources/images/image_eye_non.png");
    static readonly image_menu: ImageURISource = require("../../../resources/images/image_menu.png");
    
    static readonly comWelcome: ImageURISource = require("../../../resources/images/comWelcome.png");
    static readonly unitSleep: ImageURISource = require("../../../resources/images/unitSleep.jpg");
    static readonly unitUp: ImageURISource = require("../../../resources/images/unitUp.png");
    static readonly topLineGray: ImageURISource = require("../../../resources/images/topLineGray.png");
    static readonly topLineBlue: ImageURISource = require("../../../resources/images/topLineBlue.png");
    static readonly splash: ImageURISource = require("../../../resources/images/splash.png");
    static readonly logoWhrite: ImageURISource = require("../../../resources/images/logoWhrite.png");
    static readonly logoOrange: ImageURISource = require("../../../resources/images/logoOrange.png");
    static readonly logo: ImageURISource = require("../../../resources/images/logo.png");
    static readonly lineVolny: ImageURISource = require("../../../resources/images/lineVolny.png");
    static readonly playerBg: ImageURISource = require("../../../resources/images/fonVolny.png");
    static readonly pogruzkaVson: ImageURISource = require("../../../resources/images/pogruzkaVson.jpg");
    static readonly share: ImageURISource = require("../../../resources/images/share.jpg");
    static readonly dowload: ImageURISource = require("../../../resources/images/dowload.png");
    
    
    //categories img
    // static readonly recomendCat: ImageURISource = require("../../../resources/images/recomendCat.png");
    // static readonly baseMedAndTime: ImageURISource = require("../../../resources/images/baseMedAndTime.png");
    // static readonly stressAndTrevoga: ImageURISource = require("../../../resources/images/stressAndTrevoga.png");
    // static readonly sleepAndUp: ImageURISource = require("../../../resources/images/sleepAndUp.png");
    // static readonly proizodMysli: ImageURISource = require("../../../resources/images/proizodMysli.png");
    // static readonly lichnyiRost: ImageURISource = require("../../../resources/images/lichnyiRost.png");
    // static readonly workProductiv: ImageURISource = require("../../../resources/images/workProductiv.png");
    
}
