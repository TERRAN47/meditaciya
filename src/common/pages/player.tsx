import React from "react";
import {Alert, Text, ImageBackground, TouchableOpacity, View, ViewStyle} from "react-native";
import {WaveHeaders} from "../components/CustomHeaders";
import {styleSheetCreate} from "../utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import { NoHeader } from "../components/Headers";
import {Colors, windowHeight} from "../../core/theme";
import { connectAdv } from "../../core/store/connectAdv";
import { Dispatch } from "redux";
import { NavigationActions } from "../../navigation/navigation";
import {IAppState} from "../../core/store/appState";
import {publicUrl} from '../urls/urlPublic'
import {ImageResources} from "../ImageResources.g";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import {
  NavigationRoute,
  NavigationScreenProp,
} from "react-navigation";
import Sound from "react-native-sound"
import {CurseActionsAsync} from "../../modules/curse/curseActionsAsync";
interface IStateProps {
  curseSounds: any;
}

interface IDispatchProps {
  navigateFirstCat(): void;
  soundComplit(id: string): void;
}

interface IState {
  playStatus: boolean;
  playSeconds: number;
  duration: number;
  soundDuration: string;
}
interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    curseSounds: state.curse.curseSounds
  }),
  (dispatch: Dispatch): IDispatchProps => ({

    navigateFirstCat(): void {
      dispatch(NavigationActions.navigateToFirstCat());
    },
    soundComplit(id): void {
      //@ts-ignore
      dispatch(CurseActionsAsync.soundComplit(id));
    },
  })
)
export class SoundPage extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = NoHeader();
  sound: Sound | null
  timeout: any

  constructor(props: any) {
    super(props);

    this.state = {
      playStatus: true,
      playSeconds: 0,
      duration:0,
      soundDuration: '0'
    };

    this.sound = null
    this.timeout = null
  }

  componentWillUnmount(){
    this.playComplete
    if(this.sound){
        this.sound.release();
        this.sound = null;
    }
    const {playSeconds, duration} = this.state
    const {params} = this.props.navigation.state;
    if(params.type != "meditation" && playSeconds > 0 && duration > 0 && playSeconds >= duration-30){
      
      this.dispatchProps.soundComplit(params.sound.exerc_id);
    }
    if(this.timeout){
      clearInterval(this.timeout);
    }
  }
  playComplete = (success:any) => {
    if(this.sound){
      if (success) {
       // console.log('successfully finished playing');
      } else {
        //console.log('playback failed due to audio decoding errors');
       // Alert.alert('Внимание!', 'Идет загрузка аудио...');
      }
      this.setState({playStatus:true, playSeconds:0});
      this.sound.setCurrentTime(0);
    }
  }

  componentDidMount(){
    const {params} = this.props.navigation.state;
    let global = ""; //record ? '' : Sound.MAIN_BUNDLE
    this.sound = new Sound(params.type == "meditation" ? 
      `${publicUrl}/resources/meditations/${params.arrator}${params.sound}` 
    : 
      `${publicUrl}/resources/curses/${params.arrator}${params.sound.file_name}`, global, (e:any) => {
      if (e) {
        Alert.alert('Ошибка загрузки аудио:', e)
      }
      if(this.sound){
        const {playStatus} = this.state
        if(!playStatus || params.type == "meditation"){
          this.play()
        }
      }
    })
  }

  private changePlayStatus = ():void=>{
    const {playStatus} = this.state;
    if(playStatus){
      this.play()
    }else{
      this.pause()
    }
  }

  private pause = ():void => {
    
    if(this.sound){
      this.sound.pause();
      this.setState({playStatus:true});
      clearInterval(this.timeout);
    }
  }

  getAudioTimeString(seconds:number){
    //const h = parseInt(seconds/(60*60));
    
    if(seconds > 0){
      //@ts-ignore
      const m = parseInt(seconds%(60*60)/60);
      //@ts-ignore
      const s = parseInt(seconds%60);
  
      return ((m<10?'0'+m:m) + ':' + (s<10?'0'+s:s));
    }else{
      return "0"
    }
  }
  
  private play = ():void => {

    if(this.sound){ 
      this.sound.play(this.playComplete);
      
      if(this.sound !== null){
        const duration = this.sound.getDuration()
        const durationString = this.getAudioTimeString(duration)
        this.setState({duration, soundDuration: durationString})

        this.timeout = setInterval(():void => {
          if(this.sound){
            this.sound.getCurrentTime((seconds:number, isPlaying:boolean):void => {
              if(isPlaying){
                this.setState({playSeconds: seconds});
              }else{
                const {playStatus} = this.state
                if(!playStatus) this.pause();
              }
            })
          }
        }, 100);
        this.setState({playStatus:false}); 
      }
    }
  }

  render(): JSX.Element {
    const {params} = this.props.navigation.state;
    const {playStatus, playSeconds, soundDuration, duration} = this.state
    console.log("player", params)

    return (
      <ImageBackground 
        source={ImageResources.playerBg} 
        resizeMode="contain"  
        style={[styles.container, {backgroundColor:params.color}]}
      >
        <View style={styles.header}>
          <WaveHeaders color="green" titleColor="#fff" bg="none" title={params.title} />
        </View>

        <View style={styles.playerBlock}>
          <TouchableOpacity style={styles.playerButton} onPress={this.changePlayStatus.bind(this)}>
            <MaterialCommunityIcons name={playStatus ? "play" : "pause"} size={60} color={params.color} />
          </TouchableOpacity>
        </View>
        <Text>{playSeconds}</Text>
        <View style={styles.timeDuration}>
          <MaterialCommunityIcons name="timer" size={20} color={Colors.white} />
          <Text style={{color:Colors.white}}>{soundDuration} мин</Text>
        </View>
      </ImageBackground>
    );
  }
}

const styles = styleSheetCreate({

  container: {
    minHeight: windowHeight,
    justifyContent:'center',
    position: "relative",
    alignItems:'center',
    width:"100%"
  } as ViewStyle,
  timeDuration: {
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 20,
    borderWidth: 2,
    borderColor: Colors.white,
    padding: 10,
    position: "absolute",
    bottom: windowHeight * .1,
    left: "auto",
  } as ViewStyle,
  buttonPause:{
    position:"absolute", 
    zIndex:10, 
    left:0, 
    top:0
  } as ViewStyle,
  playerBlock: {
    borderRadius: 50,
    overflow: 'hidden'
  } as ViewStyle,
  playerButton: {
    width: 100,
    height: 100,
    justifyContent:'center',
    alignItems: "center",
    backgroundColor: Colors.white,
  } as ViewStyle,
  header: {
    position: "absolute",
    top: 30, left: 0,
    width: "100%",
  } as ViewStyle,
});