export class Pages {
    main = "main";

    login = "login";
    launch = "launch";
    registration = "registration"
    plannedRuns = "plannedRuns";
    currentRun = "currentRun";
    player = "player";
    inviteUsers = "inviteUsers";
    welcomePage = "welcomePage";
    homePage = "homePage";
    firstCategories = "firstCategories";
    curse = "curse";
    playground = "playground";
    inDeveloping = "inDeveloping";
    meditation = "meditation";
}