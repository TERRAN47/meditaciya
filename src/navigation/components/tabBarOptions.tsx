
import { Text, TextStyle } from 'react-native'
import { NavigationActions, NavigationScreenConfig, NavigationScreenOptions } from 'react-navigation'
import * as React from 'react'
//import { tabBarStyle } from '../../core/theme/commonStyles'
import { windowHeight } from '../../core/theme'
import { NavigationPages } from '../navigation'
import { localization } from '../../common/localization/localization'
import { styleSheetCreate,  } from '../../common/utils' //styleSheetFlatten
import { isIphoneX, Colors } from "../../core/theme"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"

const tabBarIcons = {
  home: ["timer", "timer"],
  homeWork: ["timer", "timer"],
} as { [key: string]: string[] }

let isLogged: boolean = false
export const setLoggedState = (state: boolean): void =>  {
  isLogged = state
};
// eventRegister.addEventListener(EventNames.login, setLoggedState.bind(null, true));
// eventRegister.addEventListener(EventNames.logout, setLoggedState.bind(null, false));

export const tabBarOptions: NavigationScreenConfig<NavigationScreenOptions> =
  ({ navigation }): NavigationScreenOptions => {
    const routeName: string = navigation.state.routeName
    return (
      {
        tabBarIcon: (iconInfo: { tintColor: string | null; focused: boolean; horizontal: boolean }):
          JSX.Element => {
            const name = tabBarIcons[routeName]
            return (
              <MaterialCommunityIcons name={name ? "" : name} size={20} color={Colors.black} />
            )
          },
         
          tabBarLabel: (options: {tintColor: string | null; focused: boolean }): React.ReactElement<any> | null => {
            // const style: any = styleSheetFlatten(
            //   [styles.label, options.focused ? {color: Colors.greenish} : {}]
            // )

            return (
              <Text style={styles.label}>{(localization.pages as any)[routeName]}</Text>
            )
          },

          tabBarOnPress: async (options: any): Promise<void> => {
            const navKey: string = options.navigation.state.key;
              if ( navKey === 'profile' ) {
                if ( !isLogged ) {
                  options.navigation.navigate({
                    routeName: 'auth',
                    action: NavigationActions.navigate({routeName: NavigationPages.login})
                  })

                  return
                }
              } else if ( navKey === 'create' ) {
                if ( !isLogged ) {
                  options.navigation.navigate({
                    routeName: 'auth',
                    action: NavigationActions.navigate({routeName: NavigationPages.login})
                  })
                  return
                }
              } else if ( navKey === 'messages' ) {
                return
              }
              options.defaultHandler();
          }
        }
      )
  }

  const styles = styleSheetCreate({
    label: {
      fontSize: isIphoneX ? windowHeight * 0.012 : windowHeight * 0.014,
  
      color: Colors.warmGrey,
      marginBottom: windowHeight * .003,
      textAlign: 'center',
    } as TextStyle
  })