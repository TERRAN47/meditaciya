import {
    createStackNavigator,
    NavigationAction,
    NavigationActions,
    NavigationState,
    StackActions
} from "react-navigation";
import {CoreActions} from "../../core/store";
import {IAppState} from "../../core/store/appState";
import {Colors, isIos} from "../../core/theme";
import {AuthActions} from "../../modules/auth/authActions";
import {LoginPage} from "../../modules/auth/LoginPage";
import {Reginstration} from '../../modules/auth/Reginstration'
import {LaunchPage} from "../../modules/auth/LaunchPage";
import {extendWithDontPushTwoPageInStack} from "../extendWithDontPushTwoPageInStack";
import {NavigationPages} from "../navigation";
import { WelcomePage } from "../../modules/home/welcomePage";
import {HomePage} from "../../modules/home"
// import {FirstCategories} from '../../modules/home/firstCats'
import {CursePage} from '../../modules/curse'
import {SoundPage} from "../../common/pages/player";
import {MeditationPage} from '../../modules/curse/meditation'
import { TabsNavigator } from "./tabsNavigator"
export const RootNavigator = createStackNavigator({
  TabsNavigator: TabsNavigator,
  [NavigationPages.launch]: {screen: LaunchPage},
  [NavigationPages.login]: {screen: LoginPage},
  [NavigationPages.registration]: {screen: Reginstration},
  [NavigationPages.welcomePage]: {screen: WelcomePage},
  [NavigationPages.homePage]: {screen: HomePage},
  //[NavigationPages.firstCategories]: {screen: FirstCategories},
 
  [NavigationPages.curse]: {screen: CursePage},
  [NavigationPages.meditation]: {screen: MeditationPage},
  [NavigationPages.player]: {screen: SoundPage},
}, {
  //headerMode: "screen",
  cardStyle: {
    backgroundColor: isIos ? Colors.white : Colors.transparent
  },
});

extendWithDontPushTwoPageInStack(RootNavigator.router);

export const RootNavigationInitialState = RootNavigator.router.getStateForAction(NavigationActions.init({}), undefined);

export function rootNavigationReducer(
  state: NavigationState = RootNavigationInitialState,
  action: NavigationAction): NavigationState {
  switch (action.type) {
    case CoreActions.rehydrate.type:
      const appState = (action as any).payload as IAppState;

      if (appState != null && appState.system.authToken) {
        return RootNavigator.router.getStateForAction(StackActions.reset(
          {
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: NavigationPages.homePage,
              })
            ]
          }
        ), state);
      }

      return {...RootNavigationInitialState};
    case AuthActions.login.done.type:
      return RootNavigator.router.getStateForAction(StackActions.reset(
        {
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: NavigationPages.homePage,
            })
          ]
        }
      ), state);
      case AuthActions.registerUser.done.type:
        return RootNavigator.router.getStateForAction(StackActions.reset(
          {
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: NavigationPages.homePage,
              })
            ]
          }
        ), state);
    default:
        return RootNavigator.router.getStateForAction(action, state);
  }
}