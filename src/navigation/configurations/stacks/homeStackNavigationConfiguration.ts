import { createStackNavigator } from 'react-navigation'
import { extendWithDontPushTwoPageInStack } from '../../extendWithDontPushTwoPageInStack'
import { NavigationPages } from '../../navigation'
import { Colors } from '../../../core/theme'
import {HomePage} from '../../../modules/home'

export const HomeStackNavigator = createStackNavigator(
  {
    [NavigationPages.homePage]: { screen: HomePage},
  },
  {
    headerMode: 'screen',
    cardStyle: { backgroundColor: Colors.greenish },
    headerLayoutPreset: 'center',
  }
)

extendWithDontPushTwoPageInStack(HomeStackNavigator.router)