import { createStackNavigator } from 'react-navigation'
import { extendWithDontPushTwoPageInStack } from '../../extendWithDontPushTwoPageInStack'
import { NavigationPages } from '../../navigation'
import { Colors } from '../../../core/theme'
import { FirstCategories } from '../../../modules/home/firstCats'

export const FirstCatStackNavigator = createStackNavigator(
  {
    [NavigationPages.firstCategories]: { screen: FirstCategories }
  },
  {
    headerMode: 'screen',
    cardStyle: { backgroundColor: Colors.grey },
    headerLayoutPreset: 'center',
  }
)

extendWithDontPushTwoPageInStack(FirstCatStackNavigator.router)