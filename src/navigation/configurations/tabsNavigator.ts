import { createBottomTabNavigator } from 'react-navigation'
import { tabBarOptions } from '../components/tabBarOptions'
import { tabBarStyle } from '../../core/theme/commonStyles'
//import { EmptyComponent } from '../../common/components/EmptyComponent'
import { HomeStackNavigator } from './stacks/homeStackNavigationConfiguration'
import { FirstCatStackNavigator } from './stacks/firstCatStackNavigationConfiguration'
// import { ConnectStackNavigator } from './stacks/connectStackNavigationConfiguration'
// import { TrainingStackNavigator } from './stacks/trainingStackNavigationConfiguration'

export const TabsNavigator = createBottomTabNavigator(
  {
    home: HomeStackNavigator,
    firstCat: FirstCatStackNavigator,
    // connect: ConnectStackNavigator,
    // training: TrainingStackNavigator,
  },
  {
    defaultNavigationOptions: tabBarOptions,
    tabBarOptions: { style: tabBarStyle.container },
  }
)
