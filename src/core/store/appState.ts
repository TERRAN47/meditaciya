import {NavigationState, DrawerNavigationState} from "react-navigation";
import {NavigationConfig} from "../../navigation/config";
import {ISystemState, SystemInitialState} from "./systemState";
import {AuthInitialState, IAuthState} from "../../modules/auth/authState";
import {HomeInitialState, IHomeState} from "../../modules/home/homeState";
import {CurseInitialState, ICurseState} from "../../modules/curse/curseState";
import {CommonInitialState, ICommonState} from "../../modules/commonState";

export interface IAppState {
  navigation: INavigationState;
  system: ISystemState;
  auth: IAuthState;
  home: IHomeState;
  curse: ICurseState;
  common: ICommonState;
}

export interface INavigationState {
  root: NavigationState;
  menu: DrawerNavigationState;
  currentRun: NavigationState;
  plannedRuns: NavigationState;
}

export function getAppInitialState(): IAppState {
  const NavigationInitialState: INavigationState = NavigationConfig.instance.getCombinedInitialState();

  return {
    navigation: NavigationInitialState,
    system: SystemInitialState,
    auth: AuthInitialState,
    home: HomeInitialState,
    curse: CurseInitialState,
    common: CommonInitialState,
  };
}