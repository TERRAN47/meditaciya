import {combineReducers} from "redux";
import {NavigationConfig} from "../../navigation/config";
import {IAppState, INavigationState} from "./appState";
import {Reducers} from "./Reducers";
import {systemReducer} from "./systemReducer";
import {authReducer} from "../../modules/auth/authReducer";
import {homeReducer} from "../../modules/home/homeReducer";
import {curseReducer} from "../../modules/curse/curseReducer";
import {commonReducer} from "../../modules/commonReducer";

export function createMainReducer(): any {
    const navigationReducers: Reducers<INavigationState> = NavigationConfig.instance.getReducer();

    const reducers: Reducers<IAppState> = {
        navigation: combineReducers(navigationReducers),
        system: systemReducer,
        auth: authReducer,
        home: homeReducer,
        curse: curseReducer,
        common: commonReducer
    };

    return combineReducers(reducers);
}