/*tslint:disable*/
import {BaseRequest} from "../BaseRequest";

export class CurseApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
    super();
    this.getCurseSounds = this.getCurseSounds.bind(this);
  }

  async getCurseSounds(curse_id: string): Promise<any> {
    try{
      const responce = await this.fetch({
        url: `get/curse/sounds/${curse_id}`, config: {
          method: "GET",
        }
      }).catch((err): any =>{
        console.log("err", err)
        BaseRequest.handleError
      });

      console.log("curseSounds", responce)

      return responce
    }catch(err){
      return BaseRequest.handleError
    }
  }
  
}