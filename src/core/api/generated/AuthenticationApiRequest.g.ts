/*tslint:disable*/
import {BaseRequest} from "../BaseRequest";
import {SignInRequestDto} from "./dto/SignInRequest.g";
import {SignInResponseDto} from "./dto/SignInResponse.g";

export class AuthenticationApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
    super();
    this.signIn = this.signIn.bind(this);
    this.registration = this.registration.bind(this);
  }

  async signIn(signInRequest: SignInRequestDto): Promise<SignInResponseDto> {
  
    const responce = await this.fetch({
      url: `auth/user`, config: {
        method: "POST",
        body: {...signInRequest}
      }}).catch((err): any =>{
      console.log("err", err)
      BaseRequest.handleError
    });
    console.log('TOKET', responce.token)
    if(responce.token){
      await BaseRequest.globalOptions.setToken(responce.token);
    }

    return responce.token
  }

  async registration(regisRequest: any): Promise<any> {//SignInRequestDto
    console.log('sregisterRequest', regisRequest)
    const responce = await this.fetch({
      url: `registration/user`, config: {
        method: "POST",
        body: {...regisRequest}
      }}).catch((err): any =>{
      console.log("errApi", err)
      BaseRequest.handleError
    });
    console.log('TOKET', responce.token)
    if(responce.token){
      await BaseRequest.globalOptions.setToken(responce.token);
    }
    return responce.token
  }
  
}