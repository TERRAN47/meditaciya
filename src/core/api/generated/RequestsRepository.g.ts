import {AuthenticationApiRequest} from "./AuthenticationApiRequest.g";
import {ProfileApiRequest} from "./ProfileApiRequest.g";
import {CommonApiRequest} from "./CommonApiRequest.g";
import {HomeApiRequest} from "./HomeApiRequest.g";
import {CurseApiRequest} from "./CurseApiRequest.g";

export class RequestsRepository {
    authenticationApiRequest = new AuthenticationApiRequest(this.baseurl);
    commonApiRequest = new CommonApiRequest(this.baseurl);
    homeApiRequest = new HomeApiRequest(this.baseurl);
    curseApiRequest = new CurseApiRequest(this.baseurl);
    profileApiRequest = new ProfileApiRequest(this.baseurl);

    constructor(private baseurl: string) {
    }
}