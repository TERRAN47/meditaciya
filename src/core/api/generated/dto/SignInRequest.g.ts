/*tslint:disable*/

export interface SignInRequestDto {
    login: string;
    password: string;
}