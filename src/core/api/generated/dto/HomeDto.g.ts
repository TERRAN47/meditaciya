export interface HomeDto {
    status: boolean
}

export interface CategoryListDto {
  [x: string]: any;
    _id: string;
    title: string;
    image: string;
    first_category: boolean;
}