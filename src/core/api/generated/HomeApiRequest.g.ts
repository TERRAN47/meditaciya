/*tslint:disable*/
import {BaseRequest} from "../BaseRequest";

export class HomeApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
    super();
    this.getHomePage = this.getHomePage.bind(this);
  }

  async getHomePage(): Promise<any> {
    try{
      const responce = await this.fetch({
        url: `get/homepage`, config: {
          method: "GET",
        }})
      console.log("homepage", responce)

      return responce
    }catch(err){
      return BaseRequest.handleError
    }
  }
  
}