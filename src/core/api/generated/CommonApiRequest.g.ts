/*tslint:disable*/
import {BaseRequest} from "../BaseRequest";

export class CommonApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
    super();
    this.getCategories = this.getCategories.bind(this);
    this.сategorySelection = this.сategorySelection.bind(this);
  }

  async getCategories(): Promise<any> {
    try{
      const responce = await this.fetch({
        url: `get/categories`, config: {
        method: "GET",
      }})
      console.log("responce", responce)
      return responce
    }catch(err){
      return BaseRequest.handleError
    }
  }

  async soundComplit(curse_id: string): Promise<any> {
    try{
      const responce = await this.fetch({
        url: `sound/complite/${curse_id}`, config: {
          method: "GET",
        }})

      console.log("categorySel", responce)
  
      return responce
    }catch(err){
      return BaseRequest.handleError
    }
  }
  
  async сategorySelection(id: string): Promise<any> {

    try{
      const responce = await this.fetch({
        url: `category/selection`, config: {
        method: "POST",
        body: {id}
      }})

      console.log("categorySel", responce)
  
      return responce
    }catch(err){
      return BaseRequest.handleError
    }

  }
  
}