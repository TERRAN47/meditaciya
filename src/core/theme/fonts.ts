import {isIos} from "./common";

export class Fonts {
    static medium = isIos ? "SFProDisplay-Medium" : "Roboto-Medium";
    static regular = 'Montserrat-Regular';
    static bold = 'Montserrat-Bold';
}