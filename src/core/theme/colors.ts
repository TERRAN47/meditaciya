export class Colors {
    static greenish = "#6EADA4";
    static paleRed = "#d34744";
    static highlightPaleRed = "#B24744";
    static azul = "#2a6ee5";
    static squash = "#f5b220";
    static blue = "#468FCC";
    static greyDark = "#565867";
    static grey = "#B3B3B3";
    static greyLight = "#E6E6E6";
    static orange = "#EBA223";
    static orangeDark = "#F57D31";
    static white = "#FFFFFF";
    static white88 = "#F5FCFF88";
    static whiteTwo = "#ebebeb";
    static whiteThree = "#f7f7f7";
    static warmGrey = "#979797";
    static pink = "#ffdcc7";
    static warmGreyTwo = "#9b9b9b";
    static charcoalGrey93 = "rgba(42,48,52,0.93)";
    static black = "#595a68";
    static dark = "#263238";

    static transparent = "transparent";
}