import {Platform, TextStyle, ImageStyle, ViewStyle} from "react-native";
import {styleSheetCreate,  styleSheetFlatten} from "../../common/utils";
import {Colors} from "./colors";
import {Fonts} from "./fonts";
import {windowWidth, windowHeight} from './index'

export const CommonStyles = styleSheetCreate({
    flex1: {
      flex: 1
    } as ViewStyle,
    flexWhiteBackground: {
      flex: 1,
      backgroundColor: Colors.white
    } as ViewStyle,
    text: {
      fontFamily:Fonts.regular,
      fontSize: windowWidth * .03
    } as TextStyle,
    textBold: {
      fontFamily:Fonts.bold,
      fontSize: windowWidth * .03
    } as TextStyle,
    bottomEmptySpace: {
        height: 50,
        backgroundColor: Colors.white
    } as ViewStyle,
    iPhoneXFooter: {
        height: 20
    } as ViewStyle,
    rowBlockAlgCenter:{
        flexDirection: 'row',
        alignItems:"center"
    } as ViewStyle,
    rowWrapCenter:{
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        alignItems:"center"
    } as ViewStyle,
});

export const titleText = {
  fontFamily:Fonts.bold,
  marginVertical: 10,
  fontSize: windowWidth * .04
} as TextStyle;

export const textDefault = {
  fontFamily:Fonts.regular,
  fontSize: windowWidth * .03
} as TextStyle

export const textBold = {
  fontFamily:Fonts.bold,
  fontSize: windowWidth * .03
} as TextStyle;
export const curseImage = {
  width: windowWidth * .3,
  minHeight: windowHeight * .12,
  borderRadius: 10,
} as ImageStyle

export const centerTextBlock = {
  paddingLeft: 5,
  marginTop: -15,
  width: windowWidth * .5,
} as ViewStyle

export const cursesItem = {
  flexDirection: "row",
  marginVertical: 10,
  alignItems: 'center',
  justifyContent: 'space-around'
} as ViewStyle;

const commonHeaderTitleStyle = {
    alignSelf: "center",
    textAlign: "center",
    fontSize: 20,
    fontFamily: Fonts.medium,
    color: Colors.white,
} as TextStyle;

const commonHeaderStyle = {
    borderBottomWidth: 0,
    borderBottomColor: Colors.transparent,
    ...Platform.select({
        ios: {
            shadowRadius: 4,
            shadowOpacity: 0.3,
            shadowOffset: {width: 0, height: 4},
        },
        android: {
            elevation: 8
        }}),
} as ViewStyle;

export const CommonHeaderStyles = styleSheetCreate({
    headerTitleStyle: styleSheetFlatten([commonHeaderTitleStyle, {color: Colors.white}]),
    headerStyle: styleSheetFlatten([commonHeaderStyle, {backgroundColor: Colors.black}]),
});

export const tabBarStyle = styleSheetCreate({
    icon: {
      height: windowWidth * .075,
      width: windowWidth * .075,
      alignSelf: 'center',
    } as ImageStyle,
    iconBig: {
      height: windowHeight * .05,
      width: windowHeight * .05,
      marginTop: windowHeight * .018,
    } as ImageStyle,
    container: {
      backgroundColor: Colors.white,
      height: windowWidth * .14,
      paddingBottom:5
    } as ViewStyle,
  })
  