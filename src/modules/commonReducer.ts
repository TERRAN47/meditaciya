import {Success} from "typescript-fsa";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../common/newState";
import {CommonActions} from "./commonActions";
import {CommonInitialState, ICommonState} from "./commonState";


function TargetCurse(state: ICommonState,  payload: Success<any, any>): ICommonState {
  return newState(state, {targetCurse: payload.params});
}

export const commonReducer = reducerWithInitialState(CommonInitialState)
.case(CommonActions.targetCurse, TargetCurse)