import {Dispatch} from "redux";
import {SimpleThunk} from "../../common/simpleThunk";
import {requestsRepository} from "../../core/api/requestsRepository";
import {CurseActions} from "./curseActions";
import {CommonActions} from "../commonActions"

export class CurseActionsAsync {

  static getCurseSounds(id: string): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
 
      try {
        dispatch( CurseActions.courseSounds.started({}));

        const responce = await requestsRepository.curseApiRequest.getCurseSounds(id);

        dispatch(CurseActions.courseSounds.done({params:responce,}));
      } catch (error) {
        console.log("error", error);
        dispatch(CurseActions.courseSounds.failed(error));
      }
    };
  }

  static soundComplit(id: string): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {

      try {
        dispatch(CurseActions.soundComplit.started({}));
        const responce = await requestsRepository.commonApiRequest.soundComplit(id);

        dispatch(CommonActions.targetCurse({params:responce}));
      } catch (error) {
        console.log("error", error);
        dispatch(CurseActions.soundComplit.failed(error));
      }
    };
  }
  

}