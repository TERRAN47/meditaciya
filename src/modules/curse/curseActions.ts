import {actionCreator} from "../../core/store";

export class CurseActions {
  static courseSounds = actionCreator.async<IEmpty, any, Error>("Course/COURSE_SOUNDS");
  static soundComplit = actionCreator.async<IEmpty, any, Error>("Course/SOUND_COMPLIT");
}

