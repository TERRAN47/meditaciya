
export interface ICurseState {
    error: string | null;
    curseSounds: any
}

export const CurseInitialState: ICurseState = {
    error: null,
    curseSounds: null
};
