import React from "react";
import {ScrollView, ImageStyle, SafeAreaView, Text, TextStyle, TouchableOpacity, View, ViewStyle, Image} from "react-native";
import {WaveHeaders} from "../../common/components/CustomHeaders";
import {styleSheetCreate} from "../../common/utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import { NoHeader } from "../../common/components/Headers";
import {Arrators} from "../../common/components/Arrators";
import { Loading } from "../../common/components/Loading";
import  {Colors, CommonStyles, windowHeight, windowWidth} from "../../core/theme";
import { connectAdv } from "../../core/store/connectAdv";
import { Dispatch } from "redux";
import { NavigationActions } from "../../navigation/navigation";
import {IAppState} from "../../core/store/appState";
import {publicUrl} from '../../common/urls/urlPublic'
import {
  NavigationRoute,
  NavigationScreenProp,
} from "react-navigation";
import {titleText, curseImage, textBold, textDefault, centerTextBlock, cursesItem} from '../../core/theme/commonStyles'
import {ImageResources} from "../../common/ImageResources.g";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import {CurseActionsAsync} from "./curseActionsAsync";
interface IStateProps {
  curseSounds: any;
  isLoading: boolean;
  targetCurse: any;
}

interface IDispatchProps {
  navigateWelcome(): void;
  navigateToPlayer(sound: any): void;
  getCurseSounds(id: string): void;
}

interface IState {
  curseSoundsState: any;
}
interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    curseSounds: state.curse.curseSounds,
    targetCurse: state.common.targetCurse,
    isLoading: state.common.loading
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateWelcome(): void {
      dispatch(NavigationActions.navigateToWelcome());
    },
    navigateToPlayer(sound): void {
      dispatch(NavigationActions.navigateToPlayer(sound));
    },
    getCurseSounds(id): void {
      //@ts-ignore
      dispatch(CurseActionsAsync.getCurseSounds(id));
    },
  })
)
export class CursePage extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = NoHeader();
  constructor(props: any) {
    super(props);

    this.state = {
      curseSoundsState: [],
    };

  }

  componentDidMount(){
    const {targetCurse} = this.stateProps;
    this.dispatchProps.getCurseSounds(targetCurse.curse_id._id);
  }

  navigateToPlayer = (sound: any): void =>{
    const {targetCurse} = this.stateProps;
    this.dispatchProps.navigateToPlayer({
      sound, 
      arrator:targetCurse.gender_sound,
      type: targetCurse.type,
      title:targetCurse.curse_id.title, 
      color:targetCurse.cat_id.color
    })
  }

  render(): JSX.Element {
    const {curseSounds, targetCurse, isLoading} = this.stateProps;

    console.log("targetCurse", targetCurse)
    return (
      <SafeAreaView style={styles.container}>
        <Loading isLoading={isLoading}  />
        <ScrollView>
          <WaveHeaders color="green" title={targetCurse.curse_id.title} />
          <View style={styles.ImageCurseBlock}>
            <Image
              source={{uri: publicUrl+'/image/exercise/'+targetCurse.curse_id.image}}
              resizeMode="cover"
              style={styles.curseImage}
            />
            <View  style={styles.timeCurse}>
              <View style={CommonStyles.rowBlockAlgCenter}>
                <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                <Text style={CommonStyles.text}>{targetCurse.curse_id.time} мин | {targetCurse.curse_id.type == "curse" && "КУРС"}</Text>
              </View>
            </View>
            <Image
              source={ImageResources.lineVolny}
              resizeMode="cover"
              style={styles.lineVolny}
            />
          </View>
          <View style={styles.content}>

            <Text style={CommonStyles.text}>{targetCurse.curse_id.description}</Text>
            <Arrators genderSound={targetCurse.gender_sound} />

            <View style={CommonStyles.rowWrapCenter}>
            {
              curseSounds && curseSounds.length > 0 &&
              curseSounds.map((item: any, index: number)=>{
                if(item.level == "one" && item.file_type == "sound"){
                  return(
                    targetCurse.count_lern >= item.number  ?
                      <View key={index} style={styles.itemSoundBlock}>
                        <TouchableOpacity 
                          onPress={this.navigateToPlayer.bind(this, item)} 
                          style={[styles.itemSound, {borderColor: Colors.greenish, backgroundColor: Colors.greenish}]}>
                          <MaterialCommunityIcons name="check" size={windowWidth * .08} color={Colors.white} />
                        </TouchableOpacity>
                        <Text style={styles.numSound}>{item.number}</Text>
                      </View>
                    :
                      targetCurse.count_lern == item.number-1  ?
                        <View key={index} style={styles.itemSoundBlock}>
                          <TouchableOpacity 
                          onPress={this.navigateToPlayer.bind(this, item)} 
                          style={[styles.itemSound, {borderColor: Colors.black, backgroundColor: Colors.black}]}>
                            <MaterialCommunityIcons name="play" size={windowWidth * .08} color={Colors.white} />
                          </TouchableOpacity>
                          <Text style={styles.numSound}>{item.number}</Text>
                        </View>
                      :
                        <View key={index} style={styles.itemSoundBlock}>
                          <View style={styles.itemSound}>
                            <MaterialCommunityIcons name="play" size={windowWidth * .08} color={Colors.grey} />
                          </View>
                          <Text style={styles.numSound}>{item.number}</Text>
                        </View>
                  )
                }else{
                  return null
                }
              })
            }
            </View>

            <View style={styles.dowloadBlock}>
              <Image
                source={ImageResources.dowload}
                resizeMode="cover"
                style={styles.dowloadImage}
              />
              <Text style={styles.dowloadTitle}>ЗАГРУЗИТЬ МЕДИТАЦИЮ</Text>
            </View>

            <View>
                <Text style={titleText}>Техники</Text>
                <View>
                  <View style={cursesItem}>
                    <Image
                      source={{uri: 'http://2.59.40.117/image/exercise/dyhanie.jpg'}}
                      resizeMode="cover"
                      style={curseImage}
                    />
                    <View>
                      <View style={centerTextBlock}>
                        <Text style={textBold}>Основы</Text>
                        <View style={CommonStyles.rowBlockAlgCenter}>
                          <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                        
                          <Text style={textDefault}> 3-10 мин | КУРС</Text>
                        </View>
                      </View>
                    </View>
                    <MaterialCommunityIcons name="chevron-right" size={35} color={Colors.greyDark} />
                  </View>
                  <View style={cursesItem}>
                    <Image
                      source={{uri: 'http://2.59.40.117/image/exercise/dyhanie.jpg'}}
                      resizeMode="cover"
                      style={curseImage}
                    />
                    <View>
                      <View style={centerTextBlock}>
                        <Text style={textBold}>Рейфрейминг одиночества</Text>
                        <View style={CommonStyles.rowBlockAlgCenter}>
                          <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                        
                          <Text style={textDefault}> 3-10 мин | КУРС</Text>
                        </View>
                      </View>
                    </View>
                    <MaterialCommunityIcons name="chevron-right" size={35} color={Colors.greyDark} />
                  </View>
                </View>
              </View>

              <View>
                <Text style={titleText}>Похожые</Text>
                {
                    // recomendations ?
                    //   <View style={styles.recomendBlock}>
                    //     {
                    //       recomendations.map((item:any, index: number)=>{
                    //         return(
                    //           <TouchableOpacity onPress={this.navigateToMeditation.bind(this, item)} key={index} style={styles.recomendItem}>
                    //             <View  style={styles.imageBlock}>
                    //               <Image
                    //                 source={{uri: publicUrl+'/image/exercise/'+item.image}}
                    //                 resizeMode="cover"
                    //                 style={styles.recomendImage}
                    //               />
                    //               <Text style={styles.recomendTitle}>{item.title}</Text>
                    //             </View>

                    //             <View style={{paddingLeft:10}}>
                    //               <View style={CommonStyles.rowBlockAlgCenter}>
                    //                 <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                                  
                    //                 <Text style={textDefault}>{item.time} мин | {item.type == "curse" ? "Курс" : "Медитация"}</Text>
                    //               </View>
                    //             </View>
                    //           </TouchableOpacity> 
                    //         )
                    //       })
                    //     }
                    //   </View>
                     
                    // :
                    //   <View></View>
                  }
              </View>
              <View style={styles.button}>
                <TouchableOpacity style={styles.buttonGrayDarck}>
                  <Text style={styles.buttonWhiteText}>СЛЕДУЮЩАЯ СЕССИЯ</Text>
                </TouchableOpacity>
              </View>
          </View>

        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = styleSheetCreate({

  container: {
    minHeight: windowHeight * .9,
    backgroundColor: Colors.greenish
  } as ViewStyle,
  buttonWhiteText:{
    textAlign: 'center',
    color: Colors.white,
    fontWeight: '500'
  } as ViewStyle,
  buttonGrayDarck:{
    backgroundColor: Colors.greyDark,
    width: windowWidth * .5,
    padding: 15,
    borderRadius: windowWidth * .25,
  } as ViewStyle,
  button:{
    alignItems: "center", 
    marginTop: 10,
    marginBottom: -25,
    zIndex: 100,
  } as ViewStyle,
  numSound: {
    textAlign: "center",
    fontSize: 16
  } as TextStyle,
  dowloadTitle: {
    textAlign: "center",
    fontWeight: '400',
    marginVertical: 10,
    fontSize: 16
  } as TextStyle,
  itemSoundBlock:{
   marginVertical: 10,
   marginHorizontal: 15
  } as ViewStyle,
  dowloadBlock:{
    marginTop: 15,
    alignItems: 'center'
  } as ViewStyle,
  itemSound: {
    width: windowWidth * .10,
    height: windowWidth * .10,
    borderRadius: windowWidth * .5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: Colors.grey,

  } as ViewStyle,
  ImageCurseBlock: {
    marginTop:20,
    position:'relative'
  } as ViewStyle,
  timeCurse:  {
    position:'absolute',
    bottom:-60,
    height:80,
    zIndex:200,
    width:'100%',
    left:0,
    alignItems:'center'
  } as ViewStyle,
  lineVolny:  {
    width: windowWidth,
    position:'absolute',
    bottom:-20,
    height:80,
    zIndex:100,
    left:0,
  } as ImageStyle,
  dowloadImage: {
    width: 50,
    height: 50
  } as ImageStyle,
  curseImage:{
    width: windowWidth,
    minHeight: windowHeight * .35,
  } as ImageStyle,
  content:{
    flex: 1,
    paddingHorizontal:10,
    backgroundColor:Colors.white,
    paddingTop:30,
    minHeight: windowHeight / 2,
    paddingBottom:100,
    alignItems:"center"
  } as ViewStyle,

});