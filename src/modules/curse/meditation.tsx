import React from "react";
import {ScrollView, ImageStyle, SafeAreaView, Text, TextStyle, TouchableOpacity, View, ViewStyle, Image} from "react-native";
import {WaveHeaders} from "../../common/components/CustomHeaders";
//import {ImageResources} from "../../common/ImageResources.g";
import {styleSheetCreate} from "../../common/utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import { NoHeader } from "../../common/components/Headers";
import {Arrators} from "../../common/components/Arrators";
import { Loading } from "../../common/components/Loading";
import  {Colors, CommonStyles, windowHeight, windowWidth} from "../../core/theme";
import { connectAdv } from "../../core/store/connectAdv";
import { Dispatch } from "redux";
import { NavigationActions } from "../../navigation/navigation";
import {IAppState} from "../../core/store/appState";
import {publicUrl} from '../../common/urls/urlPublic'
import {
  NavigationRoute,
  NavigationScreenProp,
} from "react-navigation";
import {ImageResources} from "../../common/ImageResources.g";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import {CurseActionsAsync} from "./curseActionsAsync";
interface IStateProps {
  curseSounds: any;
  isLoading: boolean;
  meditation: any;
}

interface IDispatchProps {
  navigateWelcome(): void;
  navigateToPlayer(sound: any): void;
  getCurseSounds(id: string): void;
}

interface IState {
  curseSoundsState: any;
}
interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    curseSounds: state.curse.curseSounds,
    meditation: state.common.meditation,
    isLoading: state.common.loading
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateWelcome(): void {
      dispatch(NavigationActions.navigateToWelcome());
    },
    navigateToPlayer(sound): void {
      dispatch(NavigationActions.navigateToPlayer(sound));
    },
    getCurseSounds(id): void {
      //@ts-ignore
      dispatch(CurseActionsAsync.getCurseSounds(id));
    },
  })
)
export class MeditationPage extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = NoHeader();
  constructor(props: any) {
    super(props);

    this.state = {
      curseSoundsState: [],
    };

  }

  private navigateToPlayer = (): void =>{
    const {params} = this.props.navigation.state;
    this.dispatchProps.navigateToPlayer({
      sound:params.resurse_path, 
      arrator:"Men",
      type: params.type,
      title:params.title, 
      color:params.cat_id.color
    })
  }

  render(): JSX.Element {
    const {isLoading} = this.stateProps;
    const {params} = this.props.navigation.state;
    console.log("Meditation", params)
    return (
      <SafeAreaView style={styles.container}>
        <Loading isLoading={isLoading}  />
        <View style={styles.playBlock}>
          <TouchableOpacity onPress={this.navigateToPlayer} style={styles.playButton}>
            <MaterialCommunityIcons name="play" size={20} color={Colors.white} />
            <Text  style={[CommonStyles.text, {color:Colors.white}]}>НАЧАТЬ</Text>
          </TouchableOpacity>
        </View>
        <ScrollView>
          <WaveHeaders color="green" titleColor={Colors.white} title={params.title} />
          <View style={styles.ImageCurseBlock}>
            <Image
              source={{uri: publicUrl+'/image/exercise/'+params.image}}
              resizeMode="cover"
              style={styles.curseImage}
            />
            <View  style={styles.timeCurse}>
              <View style={CommonStyles.rowBlockAlgCenter}>
                <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                <Text style={CommonStyles.text}>{params.time} мин | {params.type == "curse" && "КУРС"}</Text>
              </View>
            </View>
            <Image
              source={ImageResources.lineVolny}
              resizeMode="cover"
              style={styles.lineVolny}
            />
          </View>
          <View style={styles.content}>

            <Text style={CommonStyles.text}>{params.description}</Text>
            <Arrators genderSound={"Man"} />

            <View style={CommonStyles.rowWrapCenter}>
           
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = styleSheetCreate({

  container: {
    minHeight: windowHeight,
    position: "relative",
    backgroundColor: Colors.greenish
  } as ViewStyle,
  playButton: {
    padding:10,
    backgroundColor: Colors.black,
    width: "80%",
    borderRadius: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  } as ViewStyle,
  playBlock: {
    padding:10,
    width: windowWidth,
    alignItems: "center",
    zIndex: 300,
    position: "absolute",
    left:0, bottom: 0
  } as ViewStyle,
  numSound: {
    textAlign: "center",
    fontSize: 16
  } as TextStyle,
  itemSoundBlock:{
   marginVertical: 10,
   marginHorizontal: 15
  } as ViewStyle,
  itemSound: {
    width: windowWidth * .10,
    height: windowWidth * .10,
    borderRadius: windowWidth * .5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: Colors.grey,

  } as ViewStyle,
  ImageCurseBlock: {
    marginTop:20,
    position:'relative'
  } as ViewStyle,
  timeCurse:  {
    position:'absolute',
    bottom:-60,
    height:80,
    zIndex:200,
    width:'100%',
    left:0,
    alignItems:'center'
  } as ViewStyle,
  lineVolny:  {
    width: windowWidth,
    position:'absolute',
    bottom:-20,
    height:80,
    zIndex:100,
    left:0,
  } as ImageStyle,
  curseImage:{
    width: windowWidth,
    minHeight: windowHeight * .35,
  } as ImageStyle,
  content:{
    paddingHorizontal:10,
    minHeight: windowHeight*.5,
    backgroundColor:Colors.white,
    paddingTop:30,
    paddingBottom:100,
    alignItems:"center"
  } as ViewStyle,

});