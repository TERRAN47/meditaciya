import {Failure, Success} from "typescript-fsa";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../../common/newState";
import {CurseActions} from "./curseActions";
import {CurseInitialState, ICurseState} from "./curseState";

function getCourseSoundsStarted(state: ICurseState): ICurseState {
  return newState(state, {error: null});
}

function getCourseSoundsDone(state: ICurseState, payload: Success<any, any>): ICurseState {
  return newState(state, {
    error: null,
    curseSounds:payload.params
  })
}

function getCourseSoundsFailed(state: ICurseState, failed: Failure<any, Error>): ICurseState {
  return newState(state, {
    error: failed.error.message,
  });
}



function soundComplitStarted(state: ICurseState): ICurseState {
  return newState(state, {error: null});
}

function soundComplitFailed(state: ICurseState, failed: Failure<any, Error>): ICurseState {
  return newState(state, {
    error: failed.error.message,
  });
}

export const curseReducer = reducerWithInitialState(CurseInitialState)

  .case(CurseActions.courseSounds.started, getCourseSoundsStarted)
  .case(CurseActions.courseSounds.done, getCourseSoundsDone)
  .case(CurseActions.courseSounds.failed, getCourseSoundsFailed)

  .case(CurseActions.courseSounds.started, soundComplitStarted)
  .case(CurseActions.courseSounds.failed, soundComplitFailed)