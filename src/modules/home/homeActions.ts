import {actionCreator} from "../../core/store";

export class HomeActions {
  static welcomeStatus = actionCreator("Home/WELCOME_STATUS")
  static updateCategories = actionCreator.async<IEmpty, any, Error>("Home/UPDATE_CATEGORIES");
  static updateHomePage = actionCreator.async<IEmpty, any, Error>("Home/UPDATE_HOME_PAGE");
  static firstCat = actionCreator.async<IEmpty, any, Error>("Home/FIRST_CAT");
  
}

