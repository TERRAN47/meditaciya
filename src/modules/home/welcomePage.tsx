import React from "react";
import {ScrollView, ImageStyle, Text, TextStyle, TouchableOpacity, View, ViewStyle, Image} from "react-native";
import {ButtonDefault} from "../../common/components/ButtonDefault";
import {WaveHeaders} from "../../common/components/CustomHeaders";
import {ImageResources} from "../../common/ImageResources.g";
import {styleSheetCreate} from "../../common/utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import { NoHeader } from "../../common/components/Headers";
import {Colors, Fonts, windowHeight, windowWidth} from "../../core/theme";
import { connectAdv } from "../../core/store/connectAdv";
import { Dispatch } from "redux";
import { NavigationActions } from "../../navigation/navigation";
import {HomeActions} from "./homeActions";

interface IStateProps {

}

interface IDispatchProps {
  navToBack(): void;
  navigateToLogin(): void;
}

interface IState {
  login: string;
  password: string;
  isSecureTextEntryEnabled: boolean;
  isErrorContainerVisible: boolean;
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navToBack(): void {
      dispatch(HomeActions.welcomeStatus());
      dispatch(NavigationActions.navToBack());
    },
    navigateToLogin(): void {
      dispatch(NavigationActions.navigateToLogin());
    },
  })
)
export class WelcomePage extends BaseReduxComponent<IStateProps, IDispatchProps, IState> {
  static navigationOptions = NoHeader();

  render(): JSX.Element {

    return (
      <View style={styles.container}>
        <View style={styles.buttonsBlock}>
          <TouchableOpacity style={{width:'45%'}}  onPress={this.dispatchProps.navToBack}>
            <ButtonDefault color={Colors.white} backgroundColor={Colors.greyDark} titleButton="Пропустить" />
          </TouchableOpacity>
          <TouchableOpacity style={{width:'45%'}}>
            <ButtonDefault titleButton="Посмотреть" color={Colors.white} backgroundColor={Colors.orange} />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <WaveHeaders />
          <View style={styles.content}>
            <Image
              source={ImageResources.unitUp}
              resizeMode="contain"
              style={styles.unit}
            />

            <Text style={styles.textWelcome}>ПРИВЕТ!</Text>
            <Text style={styles.textWelcome}>ДОБРО ПОЖАЛОВАТЬ В</Text>
            <Text style={[styles.textWelcome, {color:Colors.orangeDark}]}>BRIGHTHEAD</Text>
            <Image
              source={ImageResources.comWelcome}
              resizeMode="contain"
              style={styles.comWelcome}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  buttonsBlock: {
    width: windowWidth,
    paddingHorizontal:20,
    flexDirection:'row',
    position: 'absolute',
    left: 0, bottom: 10,
    zIndex: 999,
    justifyContent: "space-between"
  } as ViewStyle,
  textWelcome: {
    fontFamily:Fonts.regular,
  } as TextStyle,
  container: {
    minHeight: windowHeight * .8,
    position: 'relative'
  } as ViewStyle,
  innerContainer: {
    marginTop: windowHeight / 5,
    flex: 1,
    justifyContent: "flex-start"
  } as ViewStyle,
  comWelcome:{
    width: windowWidth * .9,
  } as ImageStyle,
  unit:{
    width: windowHeight / 5,
    maxHeight: windowHeight / 4,
  } as ImageStyle,
  content:{
    flex: 1,
    minHeight: windowHeight / 2,
    justifyContent: "center",
    alignItems:"center"
  } as ViewStyle,
  topLine:  {
    width: windowWidth,
    position:'absolute',
    top:0,
    left:0,
    height: 130
  } as ImageStyle,

});
