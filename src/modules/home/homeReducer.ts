import {Failure, Success} from "typescript-fsa";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../../common/newState";
import {HomeActions} from "./homeActions";
import {HomeInitialState, IHomeState} from "./homeState";

function SwitchWelcomeStat(state: IHomeState): IHomeState {
  return newState(state, {welcomeStatus: false});
}

function getCategoryStarted(state: IHomeState): IHomeState {
  return newState(state, {error: null});
}
function getCategoryDone(state: IHomeState, payload: Success<any, any>): IHomeState {
  return newState(state, {
    firstcatList:payload.params,
    error: null
  })
}
function getCategoryFailed(state: IHomeState, failed: Failure<any, Error>): IHomeState {
  return newState(state, {
    error: failed.error.message,
  });
}


function selFirstCatStarted(state: IHomeState): IHomeState {
  return newState(state, {error: null});
}
function selFirstCatDone(state: IHomeState, payload: Success<any, any>): IHomeState {
  return newState(state, {
    firstSign:false,
    error: null
  })
}
function selFirstFailed(state: IHomeState, failed: Failure<any, Error>): IHomeState {
  return newState(state, {
    error: failed.error.message,
  });
}


function getHomeStarted(state: IHomeState): IHomeState {
  return newState(state, {error: null});
}
function getHomeDone(state: IHomeState, payload: Success<any, any>): IHomeState {

  return newState(state, {
    actualCurse:payload.params.actualCurse,
    recomendations:payload.params.recomendations,
    error: null
  })
}
function getHomeFailed(state: IHomeState, failed: Failure<any, Error>): IHomeState {
  return newState(state, {
    error: failed.error.message,
  });
}


export const homeReducer = reducerWithInitialState(HomeInitialState)
  .case(HomeActions.welcomeStatus, SwitchWelcomeStat)
  
  .case(HomeActions.updateCategories.started, getCategoryStarted)
  .case(HomeActions.updateCategories.done, getCategoryDone)
  .case(HomeActions.updateCategories.failed, getCategoryFailed)

  .case(HomeActions.updateHomePage.started, getHomeStarted)
  .case(HomeActions.updateHomePage.done, getHomeDone)
  .case(HomeActions.updateHomePage.failed, getHomeFailed)

  .case(HomeActions.firstCat.started, selFirstCatStarted)
  .case(HomeActions.firstCat.done, selFirstCatDone)
  .case(HomeActions.firstCat.failed, selFirstFailed)