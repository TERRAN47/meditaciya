import { ImageStyle, TextStyle,  ViewStyle} from "react-native";
import {styleSheetCreate} from "../../common/utils";
import {Fonts, Colors, windowHeight, windowWidth} from "../../core/theme";

const styles = styleSheetCreate({
    buttonsBlock: {
      flex: 1,
      width: windowWidth,
      paddingHorizontal:20,
      flexDirection:'row',
      justifyContent: "space-between"
    } as ViewStyle,
    recomendTitle:{
      backgroundColor: Colors.greyDark,
      color: Colors.white,
      position: "absolute",
      bottom: 0, left:0,
      width: windowWidth * .425,
      padding: 5,
      borderBottomRightRadius: 10,
      borderBottomLeftRadius: 10,
    } as ViewStyle,
    imageBlock:{
      position: "relative",
    } as ViewStyle,
    recomendItem:{
      position: "relative",
      margin: 5,
    } as ViewStyle,
    listCatBlock: {
      flex: 1,
    } as ViewStyle,
    buttonGray:{
      backgroundColor: Colors.greyLight,
      width: windowWidth * .5,
      padding: 15,
      borderRadius: windowWidth * .25,
    } as ViewStyle,
    buttonGrayDarck:{
      backgroundColor: Colors.greyDark,
      width: windowWidth * .5,
      padding: 15,
      borderRadius: windowWidth * .25,
    } as ViewStyle,
    buttonGrayText:{
      textAlign: 'center',
      color: Colors.orange,
      fontWeight: '500'
    } as ViewStyle,
    buttonWhiteText:{
      textAlign: 'center',
      color: Colors.white,
      fontWeight: '500'
    } as ViewStyle,
    
    recomendBlock: {
      flexDirection: "row",
  
    } as ViewStyle,
    
    shareButton:{
      alignItems: "center", 
      marginTop: 10,
      marginBottom: -25,
      zIndex: 100,
    } as ViewStyle,
    recomendImage: {
      width: windowWidth * .425,
      height: windowWidth * .3,
      borderRadius: 10,
    } as ImageStyle,
    shareImage: {
      width: windowWidth,
      zIndex: 50,
      height: windowWidth/2,
    } as ImageStyle,
    playCurseBlock: {
      position: "absolute",
      left:0, bottom:0,
      backgroundColor: Colors.pink,
      padding: 10,
      borderRadius: 15,
      flexDirection: "row",
    } as ViewStyle,
    curseImageBlock: {
      position: 'relative',
      marginTop:10
    } as ViewStyle,
  
    catblock: {
      justifyContent:"center"
    } as ViewStyle,
    textWelcome: {
      fontFamily:Fonts.regular,
    } as TextStyle,
    container: {
      minHeight: windowHeight * .9,
    } as ViewStyle,
    innerContainer: {
      marginTop: windowHeight / 5,
      flex: 1,
      justifyContent: "flex-start"
    } as ViewStyle,
    curseImage:{
      width: windowWidth * .9,
      minHeight: windowHeight * .3,
    } as ImageStyle,
    unit:{
      width: windowHeight / 5,
    } as ImageStyle,
    content:{
      flex: 1,
      minHeight: windowHeight / 2,
      justifyContent: "center",
      alignItems:"center"
    } as ViewStyle,
    topLine:  {
      width: windowWidth,
      position:'absolute',
      top:0,
      left:0,
      height: 130
    } as ImageStyle,
  
  });
  export default styles;