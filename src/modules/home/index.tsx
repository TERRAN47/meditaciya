import React from "react";
import {ScrollView, SafeAreaView, Text, TouchableOpacity, View, Image} from "react-native";
import {WaveHeaders} from "../../common/components/CustomHeaders";
//import {ImageResources} from "../../common/ImageResources.g";
import {CategoryListDto} from "../../core/api/generated/dto/HomeDto.g"
import {BaseReduxComponent} from "../../core/BaseComponent";
import { NoHeader } from "../../common/components/Headers";
//import { Loading } from "../../common/components/Loading";
import {Colors, CommonStyles} from "../../core/theme";
import { connectAdv } from "../../core/store/connectAdv";
import { Dispatch } from "redux";
import { NavigationActions } from "../../navigation/navigation";
import {IAppState} from "../../core/store/appState";
import {publicUrl} from "../../common/urls/urlPublic";
import {HomeActionsAsync} from "./homeActionsAsync";
import AntDesign from "react-native-vector-icons/AntDesign"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import {ImageResources} from "../../common/ImageResources.g";
import styles from "./styles"
import {titleText, curseImage, textBold, centerTextBlock, textDefault, cursesItem} from '../../core/theme/commonStyles'
import {CommonActions} from "../commonActions"
interface IStateProps {
  welcomeStatus: boolean;
  firstSign: boolean;
  categoryList: [CategoryListDto] | null;
  actualCurse: any;
  recomendations: any;
}

interface IDispatchProps {
  navigateWelcome(): void;
  navigateFirstCat(): void;
  navigateToCurse(): void;
  navigateToMeditation(meditation: any): void;
  getHomePage(): void;
  targetCurse(curse: any): void;
}

interface IState {
  login: string;
  password: string;
  isSecureTextEntryEnabled: boolean;
  isErrorContainerVisible: boolean;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    welcomeStatus: state.home.welcomeStatus,
    recomendations: state.home.recomendations,
    firstSign: state.home.firstSign,
    actualCurse: state.home.actualCurse,
    categoryList: state.home.categoryList
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateWelcome(): void {
      dispatch(NavigationActions.navigateToWelcome());
    },
    navigateToCurse(): void {
      dispatch(NavigationActions.navigateToCurse());
    },
    navigateToMeditation(meditation): void {
      dispatch(NavigationActions.navigateToMeditation(meditation));
    },
    targetCurse(curse): void {
      //@ts-ignore
      dispatch(CommonActions.targetCurse({params:curse}));
    },
    getHomePage(): void {
      //@ts-ignore
      dispatch(HomeActionsAsync.getHomePage());
    },
    navigateFirstCat(): void {
      dispatch(NavigationActions.navigateToFirstCat());
    },
  })
)
export class HomePage extends BaseReduxComponent<IStateProps, IDispatchProps, IState> {
  static navigationOptions = NoHeader();
  
  componentDidMount(){
    const {welcomeStatus, firstSign} = this.stateProps
    if (firstSign) {this.dispatchProps.navigateFirstCat()}
    if (welcomeStatus) {this.dispatchProps.navigateWelcome()}

    this.dispatchProps.getHomePage();
  }

  private navigateToCurse = (curse: any):void =>{
    this.dispatchProps.targetCurse(curse)
    this.dispatchProps.navigateToCurse()
  }

  private navigateToMeditation = (meditation: any):void =>{
    this.dispatchProps.navigateToMeditation(meditation)
  }
  render(): JSX.Element {
    const {actualCurse, recomendations} = this.stateProps;
    console.log("recomendations", recomendations)
    console.log("actualCurse", actualCurse)
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <WaveHeaders bg="none" logo="orange" />
          <View style={styles.content}>

            <View style={styles.listCatBlock}>

              <View>
              {
                actualCurse ?
                  <View>
                    <Text style={titleText}>
                      {actualCurse.curse_id.preview_title}
                    </Text>
                    <Text style={textDefault}>
                      {actualCurse.curse_id.preview_text}
                    </Text>
                    <View style={styles.curseImageBlock}>
                      <Image
                        source={{uri: publicUrl+'/image/exercise/'+actualCurse.curse_id.image}}
                        resizeMode="cover"
                        borderRadius={15}
                        style={styles.curseImage}
                      />
                      <TouchableOpacity 
                        style={styles.playCurseBlock}  
                        onPress={this.navigateToCurse.bind(this, actualCurse)}
                      >
                        <AntDesign name="play" size={35} color={Colors.black} />
                        <View style={{paddingLeft:10}}>
                          <Text style={textBold}>{actualCurse.curse_id.title}</Text>
                          <View style={CommonStyles.rowBlockAlgCenter}>
                            <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                          
                            <Text style={textDefault}>{actualCurse.curse_id.time} мин | {actualCurse.curse_id.type == "curse" && "КУРС"}</Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                     
                    </View>
                  </View>
                :
                  <View>

                  </View>
              }
              </View>
              <View>
                <Text style={titleText}>Рекомендации для тебя</Text>
                <View>
                  {
                    recomendations ?
                      <View style={styles.recomendBlock}>
                        {
                          recomendations.map((item:any, index: number)=>{
                            return(
                              <TouchableOpacity onPress={this.navigateToMeditation.bind(this, item)} key={index} style={styles.recomendItem}>
                                <View  style={styles.imageBlock}>
                                  <Image
                                    source={{uri: publicUrl+'/image/exercise/'+item.image}}
                                    resizeMode="cover"
                                    style={styles.recomendImage}
                                  />
                                  <Text style={styles.recomendTitle}>{item.title}</Text>
                                </View>

                                <View style={{paddingLeft:10}}>
                                  <View style={CommonStyles.rowBlockAlgCenter}>
                                    <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                                  
                                    <Text style={textDefault}>{item.time} мин | {item.type == "curse" ? "Курс" : "Медитация"}</Text>
                                  </View>
                                </View>
                              </TouchableOpacity> 
                            )
                          })
                        }
                      </View>
                     
                    :
                      <View></View>
                  }
                </View>
              </View>

              <View>
                <Text style={titleText}>Мои курсы</Text>
                <View>
                  <View style={cursesItem}>
                    <Image
                      source={{uri: 'http://2.59.40.117/image/exercise/dyhanie.jpg'}}
                      resizeMode="cover"
                      style={curseImage}
                    />
                    <View>
                      <View style={centerTextBlock}>
                        <Text style={textBold}>Основы</Text>
                        <View style={CommonStyles.rowBlockAlgCenter}>
                          <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                        
                          <Text style={textDefault}> 3-10 мин | КУРС</Text>
                        </View>
                      </View>
                    </View>
                    <MaterialCommunityIcons name="chevron-right" size={35} color={Colors.greyDark} />
                  </View>
                  <View style={cursesItem}>
                    <Image
                      source={{uri: 'http://2.59.40.117/image/exercise/dyhanie.jpg'}}
                      resizeMode="cover"
                      style={curseImage}
                    />
                    <View>
                      <View style={centerTextBlock}>
                        <Text style={textBold}>Рейфрейминг одиночества</Text>
                        <View style={CommonStyles.rowBlockAlgCenter}>
                          <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                        
                          <Text style={textDefault}> 3-10 мин | КУРС</Text>
                        </View>
                      </View>
                    </View>
                    <MaterialCommunityIcons name="chevron-right" size={35} color={Colors.greyDark} />
                  </View>
                </View>
                <View style={{alignItems: "center", marginTop: 10}}>
                  <TouchableOpacity style={styles.buttonGray}>
                    <Text style={styles.buttonGrayText}>ПОКАЗАТЬ ВСЕ (4)</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View>
                <Text style={titleText}>Недавние</Text>
                {
                    recomendations ?
                      <View style={styles.recomendBlock}>
                        {
                          recomendations.map((item:any, index: number)=>{
                            return(
                              <TouchableOpacity onPress={this.navigateToMeditation.bind(this, item)} key={index} style={styles.recomendItem}>
                                <View  style={styles.imageBlock}>
                                  <Image
                                    source={{uri: publicUrl+'/image/exercise/'+item.image}}
                                    resizeMode="cover"
                                    style={styles.recomendImage}
                                  />
                                  <Text style={styles.recomendTitle}>{item.title}</Text>
                                </View>

                                <View style={{paddingLeft:10}}>
                                  <View style={CommonStyles.rowBlockAlgCenter}>
                                    <MaterialCommunityIcons name="timer" size={20} color={Colors.black} />
                                  
                                    <Text style={textDefault}>{item.time} мин | {item.type == "curse" ? "Курс" : "Медитация"}</Text>
                                  </View>
                                </View>
                              </TouchableOpacity> 
                            )
                          })
                        }
                      </View>
                     
                    :
                      <View></View>
                  }
              </View>
            </View>
          </View>
          <View>
            <View style={styles.shareButton}>
              <TouchableOpacity style={styles.buttonGrayDarck}>
                <Text style={styles.buttonWhiteText}>ПОДЕЛИТЬСЯ</Text>
              </TouchableOpacity>
            </View>
            <Image
              source={ImageResources.share}
              resizeMode="cover"
              style={styles.shareImage}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}