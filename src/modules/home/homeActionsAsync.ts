import {Dispatch} from "redux";
import {SimpleThunk} from "../../common/simpleThunk";
import {CategoryListDto} from "../../core/api/generated/dto/HomeDto.g";
import {requestsRepository} from "../../core/api/requestsRepository";
import {HomeActions} from "./homeActions";
import { NavigationActions } from "../../navigation/navigation";
export class HomeActionsAsync {

  static getCategories(): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
 
      
      try {
        dispatch(HomeActions.updateCategories.started({}));

        const responce = await requestsRepository.commonApiRequest.getCategories();
        let fistCats = responce
        if(responce){
          fistCats = responce.filter((item: CategoryListDto): any =>{
            if(item.first_category){
              return item
            }
          })
          dispatch(HomeActions.updateCategories.done({params:fistCats, result: responce}));
        }
      } catch (error) {
        console.log("error", error);
        dispatch(HomeActions.updateCategories.failed(error));
      }
    };
  }

  static getHomePage(): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
 
      
      try {
        dispatch(HomeActions.updateHomePage.started({}));

        const responce = await requestsRepository.homeApiRequest.getHomePage();
        if(responce.actulCourse && responce.recomendations){
          dispatch(HomeActions.updateHomePage.done({params:{actualCurse:responce.actulCourse, recomendations:responce.recomendations}}));
        }
      } catch (error) {
        console.log("error", error);
        dispatch(HomeActions.updateHomePage.failed(error));
      }
    };
  }

  static сategorySelection(id: string): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
 
      
      try {
        dispatch(HomeActions.firstCat.started({}));

        const responce = await requestsRepository.commonApiRequest.сategorySelection(id);

        if(responce.data == "ok"){
          dispatch(HomeActions.firstCat.done({params:responce}));
          dispatch(NavigationActions.navToBack());
        }else{
          dispatch(NavigationActions.navToBack());
        }
      } catch (error) {
        console.log("error", error);
        dispatch(HomeActions.firstCat.failed(error));
      }
    };
  }

}