import {CategoryListDto} from '../../core/api/generated/dto/HomeDto.g'

export interface IHomeState {
    error: string | null;
    welcomeStatus: boolean;
    firstSign: boolean;
    categoryList: [CategoryListDto] | null;
    firstcatList: [CategoryListDto] | null;
    actualCurse: any;
    recomendations: any;
}

export const HomeInitialState: IHomeState = {
    welcomeStatus: true,
    error: null,
    firstSign: true,
    categoryList: null,
    firstcatList: null,
    actualCurse: null,
    recomendations: null
};