
export interface ICommonState {
    targetCurse: any;
    loading: boolean;
    meditation: any;
}

export const CommonInitialState: ICommonState = {
    targetCurse: null,
    loading: false,
    meditation: null
};