import {actionCreator} from "../../core/store";

export class SerchActions {
  static welcomeStatus = actionCreator("Home/WELCOME_STATUS")
  static updateCategories = actionCreator.async<IEmpty, any, Error>("Home/UPDATE_CATEGORIES");
}

