import {Dispatch} from "redux";
import {SimpleThunk} from "../../common/simpleThunk";
import {CategoryListDto} from "../../core/api/generated/dto/HomeDto.g";
import {requestsRepository} from "../../core/api/requestsRepository";
import {SerchActions} from "./serchActions";

export class HomeActionsAsync {

  static getCategories(): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
 
      
      try {
        dispatch(SerchActions.updateCategories.started({}));

        const responce = await requestsRepository.commonApiRequest.getCategories();
        let fistCats = responce
        if(responce){
          fistCats = responce.filter((item: CategoryListDto): any =>{
            if(item.first_category){
              return item
            }
          })
        }

        dispatch(SerchActions.updateCategories.done({params:fistCats, result: responce}));
      } catch (error) {
        console.log("error", error);
        dispatch(SerchActions.updateCategories.failed(error));
      }
    };
  }

  static сategorySelection(id: string): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
 
      
      try {
        dispatch(SerchActions.updateCategories.started({}));

        const responce = await requestsRepository.commonApiRequest.сategorySelection(id);
        let fistCats = responce
        if(responce){
          fistCats = responce.filter((item: CategoryListDto): any =>{
            if(item.first_category){
              return item
            }
          })
        }

        dispatch(SerchActions.updateCategories.done({params:fistCats, result: responce}));
      } catch (error) {
        console.log("error", error);
        dispatch(SerchActions.updateCategories.failed(error));
      }
    };
  }

}