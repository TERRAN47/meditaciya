import {Failure, Success} from "typescript-fsa";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../../common/newState";
import {SerchActions} from "./serchActions";
import {SerchInitialState, ISerchState} from "./serchState";

function SwitchWelcomeStat(state: ISerchState): ISerchState {
  return newState(state, {welcomeStatus: false});
}

function getCategoryStarted(state: ISerchState): ISerchState {
  return newState(state, {error: null});
}

function getCategoryDone(state: ISerchState, payload: Success<any, any>): ISerchState {
  return newState(state, {
    error: null
  })
}

function getCategoryFailed(state: ISerchState, failed: Failure<any, Error>): ISerchState {
  return newState(state, {
    error: failed.error.message,
  });
}

export const homeReducer = reducerWithInitialState(SerchInitialState)
  .case(SerchActions.welcomeStatus, SwitchWelcomeStat)

  .case(SerchActions.updateCategories.started, getCategoryStarted)
  .case(SerchActions.updateCategories.done, getCategoryDone)
  .case(SerchActions.updateCategories.failed, getCategoryFailed)