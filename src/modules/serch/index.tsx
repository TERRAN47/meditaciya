import React from "react";
import {ScrollView, ImageStyle, SafeAreaView, FlatList, Text, TextStyle, TouchableOpacity, View, ViewStyle, Image} from "react-native";
import {WaveHeaders} from "../../common/components/CustomHeaders";
//import {ImageResources} from "../../common/ImageResources.g";
import {CategoryListDto} from "../../core/api/generated/dto/HomeDto.g"
import {styleSheetCreate} from "../../common/utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import { NoHeader } from "../../common/components/Headers";
//import { Loading } from "../../common/components/Loading";
import {Fonts, windowHeight, windowWidth} from "../../core/theme";
import { connectAdv } from "../../core/store/connectAdv";
import { Dispatch } from "redux";
import { NavigationActions } from "../../navigation/navigation";
import {IAppState} from "../../core/store/appState";
import {publicUrl} from '../../common/urls/urlPublic'

interface IStateProps {
  welcomeStatus: boolean
  firstSign: boolean
  categoryList: [CategoryListDto] | null
}

interface IDispatchProps {
  navigateWelcome(): void;
  navigateFirstCat(): void;
}

interface IState {
  login: string;
  password: string;
  isSecureTextEntryEnabled: boolean;
  isErrorContainerVisible: boolean;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    welcomeStatus: state.home.welcomeStatus,
    firstSign: state.home.firstSign,
    categoryList: state.home.categoryList
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateWelcome(): void {
      dispatch(NavigationActions.navigateToWelcome());
    },
    navigateFirstCat(): void {
      dispatch(NavigationActions.navigateToFirstCat());
    },
  })
)
export class SerchCategories extends BaseReduxComponent<IStateProps, IDispatchProps, IState> {
  static navigationOptions = NoHeader();

  renderItem = ({ item, index }: any): JSX.Element => (
    <TouchableOpacity style={styles.catblock}>
      <Image
        source={{uri: publicUrl+'/image/'+item.image}}
        resizeMode="contain"
        style={styles.catImage}
      />
      <Text style={styles.catTitle}>{`${item.title}`}</Text>
    </TouchableOpacity>
  )

  render(): JSX.Element {
    let {categoryList} = this.stateProps
    console.log("categoryList", categoryList)
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <WaveHeaders bg="none" logo="orange" />
          <View style={styles.content}>

            <View style={styles.listCatBlock}>
              {
                categoryList ?
                  <FlatList
                    data={categoryList}
                    renderItem={this.renderItem}
                    keyExtractor={item => item._id}
                  />
                :
                  <Text>NONE</Text>
              }
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = styleSheetCreate({
  buttonsBlock: {
    flex: 1,
    width: windowWidth,
    paddingHorizontal:20,
    flexDirection:'row',
    justifyContent: "space-between"
  } as ViewStyle,
  listCatBlock: {
    flex: 1,

  } as ViewStyle,
  catTitle: {
    fontFamily:Fonts.bold,
    position: 'absolute',
    top: windowWidth * .085, left: 10,
    color: "#fff",
    maxWidth: windowHeight * .7
  } as TextStyle,
  catblock: {
    justifyContent:"center"
  } as ViewStyle,
  textWelcome: {
    fontFamily:Fonts.regular,
  } as TextStyle,
  container: {
    minHeight: windowHeight * .9
  } as ViewStyle,
  innerContainer: {
    marginTop: windowHeight / 5,
    flex: 1,
    justifyContent: "flex-start"
  } as ViewStyle,
  catImage:{
    width: windowWidth * .9,
    minHeight: windowHeight * .1
  } as ImageStyle,
  unit:{
    width: windowHeight / 5,
  } as ImageStyle,
  content:{
    flex: 1,
    minHeight: windowHeight / 2,
    justifyContent: "center",
    alignItems:"center"
  } as ViewStyle,
  topLine:  {
    width: windowWidth,
    position:'absolute',
    top:0,
    left:0,
    height: 130
  } as ImageStyle,

});