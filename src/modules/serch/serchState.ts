
export interface ISerchState {
    error: string | null;
    welcomeStatus: boolean,
    firstSign: boolean,
}

export const SerchInitialState: ISerchState = {
    welcomeStatus: true,
    error: null,
    firstSign: true,
};