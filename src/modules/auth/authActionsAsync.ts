import {Dispatch} from "redux";
import {AuthHelper} from "../../common/helpers/authHelper";
import {localization} from "../../common/localization/localization";
import {SimpleThunk} from "../../common/simpleThunk";
import {SignInRequestDto} from "../../core/api/generated/dto/SignInRequest.g";
import {requestsRepository} from "../../core/api/requestsRepository";
import {AuthActions, IAuthParams} from "./authActions";

export class AuthActionsAsync {
  static login(login: string, password: string): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      login = login.replace(/\s/g, '');
      const params: IAuthParams = {login, password};
      try {
        dispatch(AuthActions.login.started(params));
        AuthHelper.checkParams(params);
        const responce = await requestsRepository.authenticationApiRequest.signIn(params as SignInRequestDto);
        console.log(123, responce)
        dispatch(AuthActions.login.done({params, result: responce}));
      } catch (error) {
        console.log("error", error);
        const errorSource = error.message == localization.errors.invalidPhone
          ? "login"
          : error.message == localization.errors.invalidPassword
            ? "password"
            : "both";
        const errorParams: IAuthParams = {...params, errorSource};
        dispatch(AuthActions.login.failed({params: errorParams, error}));
      }
    };
  }

  static registration(request: any): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      const params: IAuthParams = {login:request.email, password:request.password};
      try { 
        AuthHelper.checkParams(params);
        dispatch(AuthActions.registerUser.started(request));
        const responce = await requestsRepository.authenticationApiRequest.registration(request);
        console.log(123, responce)
           
        dispatch(AuthActions.registerUser.done({
          params: request, result: responce
        }));
      } catch (error) {
        console.log("error", error);
        const errorSource = error.message == localization.errors.invalidPhone
          ? "login"
          : error.message == localization.errors.invalidPassword
            ? "password"
            : "both";
        const errorParams: IAuthParams = {...params, errorSource};
        dispatch(AuthActions.registerUser.failed({params: errorParams, error}));
      }
    };
  }

  static getProfile(): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(AuthActions.getProfile.started({}));

        const profile: any = [];
        dispatch(AuthActions.getProfile.done({params: {}, result: profile}));
      } catch (error) {
        dispatch(AuthActions.getProfile.failed({params: {}, error}));
      }
    };
  }
}