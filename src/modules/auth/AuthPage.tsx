import React from "react";
import {Keyboard, ScrollView, ImageStyle, LayoutAnimation, Text, TouchableOpacity, View, ViewStyle, Image} from "react-native";
import {Dispatch} from "redux";
import {AuthTextInput} from "../../common/components/AuthTextInput";
import {ErrorNotification} from "../../common/components/ErrorNotification";
import {MainButton} from "../../common/components/MainButton";
import {ButtonType} from "../../common/enums/buttonType";
import {ImageResources} from "../../common/ImageResources.g";
import {localization} from "../../common/localization/localization";
import {Ref, styleSheetCreate} from "../../common/utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import {connectAdv} from "../../core/store";
import {IAppState} from "../../core/store/appState";
import {CommonStyles, windowHeight, windowWidth} from "../../core/theme";
import {AuthActionsAsync} from "./authActionsAsync";
import {ErrorSource} from "./authState";

interface IStateProps {
    isAuthenticating: boolean;
    error: string | null;
    errorSource: ErrorSource | null;
}

interface IDispatchProps {
    login(login: string, password: string): void;
}

interface IState {
  login: string;
  password: string;
  isSecureTextEntryEnabled: boolean;
  isErrorContainerVisible: boolean;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    isAuthenticating: state.auth.isAuthenticating,
    error: state.auth.error,
    errorSource: state.auth.errorSource
  }),
  (dispatch: Dispatch<any>): IDispatchProps => ({
    login: (login: string, password: string): any => dispatch(AuthActionsAsync.login(login, password))
  })
)
export class AuthPage extends BaseReduxComponent<IStateProps, IDispatchProps, IState> {
  private passwordTextInput = new Ref<AuthTextInput>();

  constructor(props: IEmpty) {
    super(props);

    this.state = {
      login: "",
      password: "",
      isSecureTextEntryEnabled: true,
      isErrorContainerVisible: false
    };
  }

  componentDidUpdate(prevProps: any): void {
    const prevStateProps: IStateProps = prevProps.stateProps;
    if (prevStateProps.error === null && this.stateProps.error !== null) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({isErrorContainerVisible: true});
    }
  }

  render(): JSX.Element {
    const {login, password, isSecureTextEntryEnabled, isErrorContainerVisible} = this.state;
    const {isAuthenticating, error, errorSource} = this.stateProps;
    const isDisabled = login == "" || password == "" || isAuthenticating;
    const isErrorForBoth = errorSource == "both";

    return (
      <TouchableOpacity style={CommonStyles.flexWhiteBackground} onPress={Keyboard.dismiss} activeOpacity={1}>
        <ScrollView>
          <View style={styles.headerContainer}>
            <Image
              source={ImageResources.logo}
              resizeMode="contain"
              style={styles.logo}
            />
            <Text style={styles.heading}>
              {"Flexin Bonus"}
            </Text>
          </View>
          <View style={styles.innerContainer}>
            <AuthTextInput
              containerStyle={styles.input}
              label={localization.auth.login}
              value={login}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"next"}
              phoneType={true}
              onSubmitEditing={this.onInputForPasswordSubmit}
              onChangeText={this.onPhoneChange}
              isError={errorSource == "login" || isErrorForBoth}
            />
            <AuthTextInput
              containerStyle={styles.input}
              ref={this.passwordTextInput.handler}
              label={localization.auth.password}
              value={password}
              keyboardType={"default"}
              spellCheck={false}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"done"}
              secureTextEntry={isSecureTextEntryEnabled}
              icon={isSecureTextEntryEnabled ? ImageResources.image_eye : ImageResources.image_eye_non}
              onIconPress={this.onSecureTextEntryIconPress}
              onSubmitEditing={isDisabled ? undefined : this.login}
              onChangeText={this.onPasswordChange}
              isError={errorSource == "password" || isErrorForBoth}
            />
          </View>

          <View style={styles.containerTwo}>
            <MainButton
              title={localization.auth.signIn}
              buttonType={isDisabled ? ButtonType.disabled : ButtonType.positive}
              disabled={isDisabled}
              onPress={this.login}
            />
            {this.renderErrorContainer(error, isErrorContainerVisible)}
          </View>
          <TouchableOpacity>
            <Text>Как организовать «сарафанное радио» клиентов?</Text>
          </TouchableOpacity>
        </ScrollView>
      </TouchableOpacity>
    );
  }

  private renderErrorContainer = (error: string | null, isErrorContainerVisible: boolean): JSX.Element | null => {
    if (error !== null && isErrorContainerVisible) {
      return <ErrorNotification errorText={error} onOkPress={this.onErrorsOkPress}/>;
    } else {
      return null;
    }
  };

  private onErrorsOkPress = (): void => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({isErrorContainerVisible: false});
  };

  private onPhoneChange = (login: string): void => {
    this.setState({login});
  };

  private onPasswordChange = (password: string): void => {
    this.setState({password});
  };

  private onInputForPasswordSubmit = (): void => {
    this.passwordTextInput.ref.onSetFocus();
  };

  private onSecureTextEntryIconPress = (): void => {
    this.setState({isSecureTextEntryEnabled: !this.state.isSecureTextEntryEnabled});
  };

  private login = (): void => {
    Keyboard.dismiss();
    this.dispatchProps.login(this.state.login, this.state.password);
  };
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    justifyContent: "flex-end"
  } as ViewStyle,
  innerContainer: {
    marginTop: windowHeight / 5,
    flex: 1,
    justifyContent: "flex-start"
  } as ViewStyle,
  input: {
    marginHorizontal: windowWidth / 14,
    marginTop: windowHeight / 25,
  } as ViewStyle,
  logo: {
    width: windowHeight / 10,
    height: windowHeight / 10
  } as ImageStyle,
  headerContainer: {
    marginTop: windowHeight / 40,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  } as ViewStyle,
  heading: {
    marginTop: windowHeight / 25,
    fontSize: 20,
  } as ViewStyle,
  containerTwo: {
    marginBottom: windowHeight / 8,
    flexDirection: "column",
    marginTop: windowWidth * .1,
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: windowWidth / 8
  } as ViewStyle,
});
