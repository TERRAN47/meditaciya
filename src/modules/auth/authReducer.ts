import {Failure, Success} from "typescript-fsa";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../../common/newState";
import {CoreActions} from "../../core/store";
import {AuthActions, IAuthParams} from "./authActions";
import {AuthInitialState, IAuthState} from "./authState";

function rehydrate(): IAuthState {
  return {...AuthInitialState};
}

function loginByEmailStarted(state: IAuthState): IAuthState {
  return newState(state, {error: null, errorSource: null, isAuthenticating: false});
}

function loginByEmailDone(state: IAuthState, payload: Success<any, any>): IAuthState {
  return newState(state, {
    error: null, 
    errorSource: null,
    isAuthenticating: true
  });
}

function loginByEmailFailed(state: IAuthState, failed: Failure<IAuthParams, Error>): IAuthState {
  return newState(state, {
    error: failed.error.message,
    isAuthenticating: false,
    errorSource: failed.params.errorSource
  });
}

function registrationStarted(state: IAuthState): IAuthState {
  return newState(state, {error: null, errorSource: null, isAuthenticating: false});
}

function registrationDone(state: IAuthState, payload: Success<any, any>): IAuthState {
  return newState(state, {
    isAuthenticating: true,
    errorSource: null,
    error: null
  })
}

function registrationFailed(state: IAuthState, failed: Failure<IAuthParams, Error>): IAuthState {
  return newState(state, {
    error: failed.error.message,
    isAuthenticating: false,
    errorSource: failed.params.errorSource
  });
}

export const authReducer = reducerWithInitialState(AuthInitialState)
  .case(CoreActions.rehydrate, rehydrate)
  .case(AuthActions.login.started, loginByEmailStarted)
  .case(AuthActions.login.done, loginByEmailDone)
  .case(AuthActions.login.failed, loginByEmailFailed)

  .case(AuthActions.registerUser.started, registrationStarted)
  .case(AuthActions.registerUser.done, registrationDone)
  .case(AuthActions.registerUser.failed, registrationFailed)
;