import React from "react";
import {Keyboard, ScrollView, LayoutAnimation, TouchableWithoutFeedback, Text, TouchableOpacity, View, ViewStyle} from "react-native";
import {Dispatch} from "redux";
import {AuthTextInput} from "../../common/components/AuthTextInput";
import {ErrorNotification} from "../../common/components/ErrorNotification";
import {ButtonDefault} from "../../common/components/ButtonDefault";
import {NoHeader} from "../../common/components/Headers";
import {WaveHeaders} from "../../common/components/CustomHeaders";

import {ImageResources} from "../../common/ImageResources.g";
import {Ref, styleSheetCreate} from "../../common/utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import {connectAdv} from "../../core/store";
import {IAppState} from "../../core/store/appState";
import {Colors, Fonts, windowHeight, windowWidth} from "../../core/theme";
import {AuthActionsAsync} from "./authActionsAsync";
import {ErrorSource} from "./authState";
import { NavigationActions } from "../../navigation/navigation";

interface IStateProps {
  isAuthenticating: boolean;
  error: string | null;
  errorSource: ErrorSource | null;
}

interface IDispatchProps {
  authIn(email: string, password: string): void;
  navigateToRegistration(): void;
}

interface IState {
  login: string;
  password: string;
  isSecureTextEntryEnabled: boolean;
  isErrorContainerVisible: boolean;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    isAuthenticating: state.auth.isAuthenticating,
    error: state.auth.error,
    errorSource: state.auth.errorSource
  }),
  (dispatch: Dispatch<any>): IDispatchProps => ({
    authIn: (email: string, password: string): any => dispatch(AuthActionsAsync.login(email, password)),
    navigateToRegistration(): void {
      dispatch(NavigationActions.navigateToRegistration());
    },
  })
)
export class LoginPage extends BaseReduxComponent<IStateProps, IDispatchProps, IState> {
  static navigationOptions = NoHeader();
  private passwordTextInput = new Ref<AuthTextInput>();

  constructor(props: IEmpty) {
    super(props);

    this.state = {
      login: "",
      password: "",
      isSecureTextEntryEnabled: true,
      isErrorContainerVisible: false
    };
  }

  componentDidUpdate(prevProps: any): void {
    const prevStateProps: IStateProps = prevProps.stateProps;
    if (prevStateProps.error === null && this.stateProps.error !== null) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({isErrorContainerVisible: true});
    }
  }

  render(): JSX.Element {
    const {login, password, isSecureTextEntryEnabled, isErrorContainerVisible} = this.state;
    const {isAuthenticating, error, errorSource} = this.stateProps;
    const isDisabled = login == "" || password == "" || isAuthenticating;
    const isErrorForBoth = errorSource == "both";

    return (
     
      
      <ScrollView>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <WaveHeaders title="Авторизация" />
          <View style={styles.content}>
            <AuthTextInput
              containerStyle={styles.input}
              label="Email"
              value={login}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"next"}
              onSubmitEditing={this.onInputForPasswordSubmit}
              onChangeText={this.onEmailChange}
              isError={errorSource == "login" || isErrorForBoth}
            />

            <AuthTextInput
              containerStyle={styles.input}
              ref={this.passwordTextInput.handler}
              label="Пароль"
              value={password}
              keyboardType={"default"}
              spellCheck={false}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"done"}
              secureTextEntry={isSecureTextEntryEnabled}
              icon={isSecureTextEntryEnabled ? ImageResources.image_eye : ImageResources.image_eye_non}
              onIconPress={this.onSecureTextEntryIconPress}
              onSubmitEditing={isDisabled ? undefined : this.login}
              onChangeText={this.onPasswordChange}
              isError={errorSource == "password" || isErrorForBoth}
            />

            <View style={styles.centerContent}>
              <TouchableOpacity style={{width:'60%', marginTop:20}}  onPress={this.login}>
                <ButtonDefault color={Colors.white} backgroundColor={Colors.greyDark} titleButton="Авторизоваться" />
              </TouchableOpacity>
              {this.renderErrorContainer(error, isErrorContainerVisible)}
              <Text style={{fontFamily: Fonts.regular, margin:20}}>ИЛИ</Text>
              <TouchableOpacity style={{width:'90%'}}  onPress={this.dispatchProps.navigateToRegistration}>
                <ButtonDefault color={Colors.white} backgroundColor={Colors.blue} titleButton="Вход через FaceBook" />
              </TouchableOpacity>
            </View>

          </View>
        </View>
        </TouchableWithoutFeedback>
      </ScrollView>

    );
  }
 private renderErrorContainer = (error: string | null, isErrorContainerVisible: boolean): JSX.Element | null => {
    if (error !== null && isErrorContainerVisible) {
      return <ErrorNotification errorText={error} onOkPress={this.onErrorsOkPress}/>;
    } else {
      return null;
    }
  };

  private onErrorsOkPress = (): void => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({isErrorContainerVisible: false});
  };

  private onEmailChange = (login: string): void => {
    this.setState({login});
  };

  private onPasswordChange = (password: string): void => {
    this.setState({password});
  };

  private onInputForPasswordSubmit = (): void => {
    this.passwordTextInput.ref.onSetFocus();
  };

  private onSecureTextEntryIconPress = (): void => {
    this.setState({isSecureTextEntryEnabled: !this.state.isSecureTextEntryEnabled});
  };

  private login = (): void => {
    Keyboard.dismiss();
    this.dispatchProps.authIn(this.state.login, this.state.password);
  };
}

const styles = styleSheetCreate({
  content: {
    flex:1,
    marginTop:windowHeight * .15,
    minHeight: windowHeight * .7,
    backgroundColor: Colors.white,
    zIndex:100,
  } as ViewStyle,
  container: {
    minHeight: windowHeight * .9
  } as ViewStyle,
  centerContent:{
    alignItems:'center'
  } as ViewStyle,
  input: {
    marginHorizontal: windowWidth / 14,
    marginTop: windowHeight / 25,
  } as ViewStyle,

});
