import React from "react";
import {ScrollView, ImageStyle, TouchableOpacity, View, ViewStyle, Image} from "react-native";
import {ButtonDefault} from "../../common/components/ButtonDefault";
import {WaveHeaders} from "../../common/components/CustomHeaders";
import {ImageResources} from "../../common/ImageResources.g";
import {styleSheetCreate} from "../../common/utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import { NoHeader } from "../../common/components/Headers";
import {Colors, windowHeight, windowWidth} from "../../core/theme";
import { connectAdv } from "../../core/store/connectAdv";
import { Dispatch } from "redux";
import { NavigationActions } from "../../navigation/navigation";

interface IStateProps {

}

interface IDispatchProps {
  navigateToRegistration(): void;
  navigateToLogin(): void;
}

interface IState {
  login: string;
  password: string;
  isSecureTextEntryEnabled: boolean;
  isErrorContainerVisible: boolean;
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToRegistration(): void {
      dispatch(NavigationActions.navigateToRegistration());
    },
    navigateToLogin(): void {
      dispatch(NavigationActions.navigateToLogin());
    },
  })
)
export class LaunchPage extends BaseReduxComponent<IStateProps, IDispatchProps, IState> {
  static navigationOptions = NoHeader();

  render(): JSX.Element {

    return (
      <View style={styles.container}>
        <ScrollView>
          <WaveHeaders />
          <View style={styles.content}>
            <Image
              source={ImageResources.unitSleep}
              resizeMode="contain"
              style={styles.unit}
            />
            <View style={styles.buttonsBlock}>
              <TouchableOpacity style={{width:'45%'}}  onPress={this.dispatchProps.navigateToRegistration}>
                <ButtonDefault color={Colors.white} backgroundColor={Colors.greyDark} titleButton="Регистрация" />
              </TouchableOpacity>
              <TouchableOpacity style={{width:'45%'}}  onPress={this.dispatchProps.navigateToLogin}>
                <ButtonDefault titleButton="Вход" color={Colors.white} backgroundColor={Colors.orange} />
              </TouchableOpacity>
            </View>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  buttonsBlock: {
    flex: 1,
    width: windowWidth,
    paddingHorizontal:20,
    flexDirection:'row',
    justifyContent: "space-between"
  } as ViewStyle,
  container: {
    minHeight: windowHeight
  } as ViewStyle,
  innerContainer: {
    marginTop: windowHeight / 5,
    flex: 1,
    justifyContent: "flex-start"
  } as ViewStyle,
  unit:{
    width: windowHeight / 5,
  } as ImageStyle,
  content:{
    flex: 1,
    minHeight: windowHeight / 2,
    justifyContent: "center",
    alignItems:"center"
  } as ViewStyle,
  topLine:  {
    width: windowWidth,
    position:'absolute',
    top:0,
    left:0,
    height: 130
  } as ImageStyle,

});
