import React from "react";
import {Keyboard, ScrollView, LayoutAnimation, Text, TouchableOpacity, TouchableWithoutFeedback, View, ViewStyle} from "react-native";
import {ButtonDefault} from "../../common/components/ButtonDefault";
import {AuthTextInput} from "../../common/components/AuthTextInput";
import {ImageResources} from "../../common/ImageResources.g";
import {Ref, styleSheetCreate} from "../../common/utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import { NoHeader } from "../../common/components/Headers";
import {WaveHeaders} from "../../common/components/CustomHeaders";
import {Fonts, Colors, windowHeight, windowWidth} from "../../core/theme";
import { connectAdv } from "../../core/store/connectAdv";
import { Dispatch } from "redux";
import {AuthActionsAsync} from "./authActionsAsync";
import { CheckBox } from 'react-native-elements'
import {ErrorNotification} from "../../common/components/ErrorNotification";
import {IAppState} from "../../core/store/appState";
import {ErrorSource} from "./authState";
interface IStateProps {
  isAuthenticating: boolean;
  error: string | null;
  errorSource: ErrorSource | null;
}

interface IDispatchProps {
  registration(params:any): void;

}

interface IState {
  login: string;
  email: string;
  password: string;
  passwordTwo: string;
  isSecureTextEntryEnabled: boolean;
  isErrorContainerVisible: boolean;
  checked: boolean;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    isAuthenticating: state.auth.isAuthenticating,
    error: state.auth.error,
    errorSource: state.auth.errorSource
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    registration: (params:any): void =>{
      //@ts-ignore
      dispatch(AuthActionsAsync.registration(params))
    }
  })
)
export class Reginstration extends BaseReduxComponent<IStateProps, IDispatchProps, IState> {
  static navigationOptions = NoHeader();
  private passwordTextInput = new Ref<AuthTextInput>();
  private passwordTwoTextInput = new Ref<AuthTextInput>();
  private emailTextInput = new Ref<AuthTextInput>();
  
  constructor(props: IEmpty) {
    super(props);

    this.state = {
      login: "",
      email: "",
      password: "",
      passwordTwo: "",
      isSecureTextEntryEnabled: true,
      isErrorContainerVisible: false,
      checked: false
    };
  }
  componentDidUpdate(prevProps: any): void {
    const prevStateProps: IStateProps = prevProps.stateProps;
    if (prevStateProps.error === null && this.stateProps.error !== null) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({isErrorContainerVisible: true});
    }
  }
  render(): JSX.Element {
    const {login, password, email, passwordTwo, checked, isErrorContainerVisible, isSecureTextEntryEnabled} = this.state;
    const {isAuthenticating, error, errorSource} = this.stateProps;
    const isDisabled = login == "" || password == "" || passwordTwo == "" || isAuthenticating;
    const isErrorForBoth = errorSource == "both";

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ScrollView>
          <WaveHeaders title="Регистрация" />
          <View style={styles.container}>

            <AuthTextInput
              containerStyle={styles.input}
              label="Имя"
              value={login}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"next"}
              onSubmitEditing={this.onInputForEmailSubmit}
              onChangeText={this.onLoginChange}
            />
            <AuthTextInput
              containerStyle={styles.input}
              ref={this.emailTextInput.handler}
              label="Email"
              value={email}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"next"}
              onSubmitEditing={this.onInputForPasswordSubmit}
              onChangeText={this.onEmailChange}
              isError={errorSource == "login" || isErrorForBoth}
            />
            <AuthTextInput
              containerStyle={styles.input}
              ref={this.passwordTextInput.handler}
              label="Пароль"
              value={password}
              keyboardType={"default"}
              spellCheck={false}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"next"}
              isError={errorSource == "password" || isErrorForBoth}
              secureTextEntry={isSecureTextEntryEnabled}
              icon={isSecureTextEntryEnabled ? ImageResources.image_eye : ImageResources.image_eye_non}
              onIconPress={this.onSecureTextEntryIconPress}
              onSubmitEditing={this.onInputForPasswordTwoSubmit}
              onChangeText={this.onPasswordChange}
            />
            <AuthTextInput
              containerStyle={styles.input}
              ref={this.passwordTwoTextInput.handler}
              label="Повторить пароль"
              value={passwordTwo}
              keyboardType={"default"}
              isError={errorSource == "password" || isErrorForBoth}
              spellCheck={false}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"done"}
              secureTextEntry={isSecureTextEntryEnabled}
              onIconPress={this.onSecureTextEntryIconPress}
              onSubmitEditing={isDisabled ? undefined : this.registration}
              onChangeText={this.onPasswordTwoChange}
            />
            <View style={styles.centerContent}>
              <CheckBox
                checked={checked}
                onPress={() => this.setState({checked: !checked})}
              />
              <Text style={{fontFamily: Fonts.regular}}>Регулярно получать информацию о Brighthead</Text>
              {this.renderErrorContainer(error, isErrorContainerVisible)}
              <TouchableOpacity style={{width:'60%', marginTop:20}}  onPress={this.registration}>
                <ButtonDefault color={Colors.white} backgroundColor={Colors.greyDark} titleButton="Регистрация" />
              </TouchableOpacity>
              <Text style={{fontFamily: Fonts.regular, margin:20}}>ИЛИ</Text>
              <TouchableOpacity style={{width:'90%'}}  onPress={this.registration}>
                <ButtonDefault color={Colors.white} backgroundColor={Colors.blue} titleButton="Регистрация через FaceBook" />
              </TouchableOpacity>
            </View>

          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
    );
  }
  private renderErrorContainer = (error: string | null, isErrorContainerVisible: boolean): JSX.Element | null => {
    if (error !== null && isErrorContainerVisible) {
      return <ErrorNotification errorText={error} onOkPress={this.onErrorsOkPress}/>;
    } else {
      return null;
    }
  };
  private onErrorsOkPress = (): void => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({isErrorContainerVisible: false});
  };
  private onLoginChange = (login: string): void => {
    this.setState({login});
  };
  private onEmailChange = (email: string): void => {
    this.setState({email});
  };

  private onPasswordTwoChange = (passwordTwo: string): void => {
    this.setState({passwordTwo});
  };
  private onPasswordChange = (password: string): void => {
    this.setState({password});
  };
  private onSecureTextEntryIconPress = (): void => {
    this.setState({isSecureTextEntryEnabled: !this.state.isSecureTextEntryEnabled});
  };
  private onInputForPasswordSubmit = (): void => {
    this.passwordTextInput.ref.onSetFocus();
  };
  private onInputForPasswordTwoSubmit = (): void => {
    this.passwordTwoTextInput.ref.onSetFocus();
  };
  private onInputForEmailSubmit = (): void => {
    this.emailTextInput.ref.onSetFocus();
  };
  private registration = (): void => {
   //Keyboard.dismiss();
    this.dispatchProps.registration({
      login: this.state.login,
      password: this.state.password,
      email:this.state.email,
      getMeInfo: this.state.checked
    });
  };
}

const styles = styleSheetCreate({
  container: {
    flex:1,
    zIndex:900,
    minHeight: windowHeight * .85,
  } as ViewStyle,
  centerContent:{
    alignItems:'center'
  } as ViewStyle,
  input: {
    marginHorizontal: windowWidth / 14,
    marginTop: windowHeight / 25,
  } as ViewStyle,
});
