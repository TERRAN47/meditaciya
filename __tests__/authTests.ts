/// <reference types="jest" />

import {AuthHelper} from "../src/common/helpers/authHelper";
import {localization} from "../src/common/localization/localization";

describe("Auth tests", () => {
    test("Wrong email format", () => {
        expect(() => {
            AuthHelper.checkParams({login: "", password: "123456"});
        })
            .toThrowError(localization.errors.invalidEmail);
    });

    test("Wrong password format", () => {
        expect(() => {
            AuthHelper.checkParams({login: "", password: "12345"});
        })
            .toThrowError(localization.errors.invalidPassword);
    });
});
